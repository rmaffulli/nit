function [PID] = PID(Y, X1, X2, varargin)
%%% *function [PID] = PID(Y, X1, X2, varargin)*
%%%
%%% ### Description
%%% This function computes the partial information decomposition of X1 and
%%% X2 about Y
%%%
%%% ### Inputs:
%%% - *Y*: must be an array of *nDimensions X nTrials* elements representing the first signal
%%% - *X1*: must be an array of *nDimensions X nTrials* elements representing the second signal
%%% - *X2*: must be an array of *nDimensions X nTrials* elements representing the third signal
%%% - *opts*: options used to calculate PID (see further notes).
%%%
%%% ### Outputs:
%%% - *PID_SR_C*: data structure containing naive, and bias corrected (if required) estimation of the PID decomposition of $(X_1;X_2)$ with target $Y$. If calculated, the structure contains as well the btsp estimate. In case of multiple bootstrapping options (i.e. in case of `length(opts.btsp_variables) > 1`) multiple bootstrap estimates are returned. 
%%%
%%% ### Further notes:
%%% The *opts* structure can have the following fields:
%%%
%%% | field                                | description                                                                                                                                                                                                                                                                      | allowed values                                                                                                                                                                                                                                                              | default   |
%%% |--------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------|
%%% | opts.bias                            | specifies the bias correction method                                                                                                                                                                                                                                             | `'naive'` (no bias correction)<br>`'le'` (linear extrapolation)<br>`'qe'`(quadratic exrapolation)                                                                                                                                                                           | `'naive'` |
%%% | opts.bin_methodX1                    | specifies the binning method for the X1 signal                                                                                                                                                                                                                                   | `'none'` (no binning)<br>`'eqpop'` (evenly populated binning)<br>`'eqspace'`(evenly spaced binning)<br>`'ceqspace'`(centered evenly spaced binning)<br>`'gseqspace'`(gaussian evenly spaced binning)<br> See the documentation of [[binr|binr]] function for more details   | `'eqpop'` |
%%% | opts.bin_methodX2                    | specifies the binning method for the X2 signal                                                                                                                                                                                                                                   | `'none'` (no binning)<br>`'eqpop'` (evenly populated binning)<br>`'eqspace'`(evenly spaced binning)<br>`'ceqspace'`(centered evenly spaced binning)<br>`'gseqspace'`(gaussian evenly spaced binning)<br> See the documentation of [[binr|binr]] function for more details   | `'eqpop'` |
%%% | opts.bin_methodY                     | specifies the binning method for the Y signal                                                                                                                                                                                                                                    | `'none'` (no binning)<br>`'eqpop'` (evenly populated binning)<br>`'eqspace'`(evenly spaced binning)<br>`'ceqspace'`(centered evenly spaced binning)<br>`'gseqspace'`(gaussian evenly spaced binning)<br> See the documentation of [[binr|binr]] function for more details   | `'eqpop'` |
%%% | opts.max_draws_per_split_number      | specifies the maximum number of draws on which to calculate the unbiased estimate of the PID. E.g. `opts.max_draws_per_split_number = 10`, the II is calculated as average on 20 trials for 2 splits and 10 trials for 1 split. (ignored if opts.bias = 'naive')                 | int > 0                                                                                                                                                                                                                                                                     | 50        |
%%% | opts.n_binsX1                        | number of bins to be used to reduce the dimensionality of X1                                                                                                                                                                                                                     | int > 1                                                                                                                                                                                                                                                                     | 3         |
%%% | opts.n_binsX2                        | number of bins to be used to reduce the dimensionality of X2                                                                                                                                                                                                                     | int > 1                                                                                                                                                                                                                                                                     | 3         |
%%% | opts.n_binsY                         | number of bins to be used to reduce the dimensionality of Y                                                                                                                                                                                                                      | int > 1                                                                                                                                                                                                                                                                     | 3         |
%%% | opts.btsp                            | number of bootstrap operations to perform for significance testing (those will be performed independently on each of the variables listed in `opts.btsp_variables`)                                                                                                              | int >= 0                                                                                                                                                                                                                                                                    | 0         |
%%% | opts.btsp_variables                  | list of variables to be bootstrapped, specified as a cell array of strings. If multiple bootstrapping operations are requested, `opts.btsp_variables` can contain multiple variables. In this case a `opts.btsp_type` option should be specified for each `opts.btsp_variables`  | cell array containing one (or more) strings (`"X1"`, `"X2"` or `"Y"`) corresponding to all variables to be bootstrapped                                                                                                                                                     | N/A       |
%%% | opts.btsp_type                       | type of bootstrapping to be applied for each variable (`'all'` shuffles all values of the corresponding variable in `opts.btsp_variables` across trials, while `'$VAR$conditioned'` shuffles trials by conditioning on values of the variable specified in the substring `$VAR$`)| cell array of same size of opts.btsp_variables, each element contains the type of shuffling to be performed for the variable (either "all"` or `"X1conditioned"`, `"X2conditioned"`, or `"Yconditioned"`)                                                                   | N/A       |
%%%
%%% The bootstrapping options allow the user to tailor the bootstrap
%%% operation to her/his specific needs. One should bear in mind that
%%% unconditioned bootstrapping (`opts.btsp_type = {'all'}`) of one
%%% variable (e.g. `Y`), destroys all MI between `(X1;Y)` as well as the MI
%%% between `(X2;Y)`. The use of a conditioned shuffling (`opts.btsp_type =
%%% {'conditioned'}`), instead, preserves MI values but destroys the
%%% correlations between the other two variables (`X1` and `X2` in the
%%% given example of shuffling `Y`). This can be useful when some PID atoms
%%% can be due to correlations between two of the three variables.
%%%
%
%  This source code is part of:
%  NIT - Neuroscience Information Toolbox
%  Copyright (C) 2020  Roberto Maffulli, Miguel Angel Casal Santiago
%
%  This program is free software: you can redistribute it and/or modify
%  it under the terms of the GNU General Public License as published by
%  the Free Software Foundation, either version 3 of the License, or
%  (at your option) any later version.
%
%  This program is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%  GNU General Public License for more details.
% 
%  You should have received a copy of the GNU General Public License
%  along with this program.  If not, see <https://www.gnu.org/licenses/>.


% check inputs dimensions
assert(length(Y(1,:)) == length(X1(1,:)) && length(Y(1,:)) == length(X2(1,:)),...
    "Number of trials are differing in X1, X2 and Y");

% check for nans
if any(isnan(Y))
    error("Y contains NaNs. Aborting.")
end
if any(isnan(X1))
    error("X1 contains NaNs. Aborting.")
end
if any(isnan(X2))
    error("X2 contains NaNs. Aborting.")
end

% set default options
if nargin == 3
    opts.bin_methodX1 = 'eqpop';
    opts.bin_methodX2 = 'eqpop';
    opts.bin_methodY = 'eqpop';
    opts.n_binsX1 = 3;
    opts.n_binsX2 = 3;
    opts.n_binsY = 3;
    opts.max_draws_per_split_number = 200;
    opts.btsp = 0;
    opts.btsp_variables = {};
    opts.btsp_type = {};
else
    opts = varargin{1};
    % check bias reduction method and set default if not provided
    if ~isfield(opts,'bias')
        opts.bias = 'naive';
    else
        assert(strcmp(opts.bias,'naive') ||...
            strcmp(opts.bias, 'le') ||...
            strcmp(opts.bias, 'qe'),...
            ['opts.bias argument can be only ''naive'', ''le'' or ''qe''. ',...
            'Specified value ''%s'''], opts.bias);
    end
    % check draws_per_split_number and set default if not provided
    if ~isfield(opts,'max_draws_per_split_number')
        opts.max_draws_per_split_number = 50;
    else
        assert(opts.max_draws_per_split_number > 0,...
            'opts.max_draws_per_split_number should be > 0');
    end 
    % check binning method and set default if not provided
    if isfield(opts,'bin_methodX1')
        assert(strcmp(opts.bin_methodX1,'none') ||...
            strcmp(opts.bin_methodX1,'eqpop') ||...
            strcmp(opts.bin_methodX1, 'eqspace') ||...
            strcmp(opts.bin_methodX1, 'ceqspace') ||...
            strcmp(opts.bin_methodX1, 'gseqspace'),...
            ['opts.bin_methodX1 argument can be only ''eqpop'', ''eqspace'', ''ceqspace'' or ''gseqspace''. ',...
            'Specified value ''%s'''], opts.bin_methodX1);
    else
        opts.bin_methodX1 = 'none';
    end
    if isfield(opts,'bin_methodX2')
        assert(strcmp(opts.bin_methodX2,'none') ||...
            strcmp(opts.bin_methodX2,'eqpop') ||...
            strcmp(opts.bin_methodX2, 'eqspace') ||...
            strcmp(opts.bin_methodX2, 'ceqspace') ||...
            strcmp(opts.bin_methodX2, 'gseqspace'),...
            ['opts.bin_methodX1 argument can be only ''eqpop'', ''eqspace'', ''ceqspace'' or ''gseqspace''. ',...
            'Specified value ''%s'''], opts.bin_methodX2);
    else
        opts.bin_methodX2 = 'none';
    end
    if isfield(opts,'bin_methodY')
        assert(strcmp(opts.bin_methodY,'none') ||...
            strcmp(opts.bin_methodY,'eqpop') ||...
            strcmp(opts.bin_methodY, 'eqspace') ||...
            strcmp(opts.bin_methodY, 'ceqspace') ||...
            strcmp(opts.bin_methodY, 'gseqspace'),...
            ['opts.bin_method argument can be only ''eqpop'', ''eqspace'', ''ceqspace'' or ''gseqspace''. ',...
            'Specified value ''%s'''], opts.bin_methodY);
    else
        opts.bin_methodY = 'none';
    end
    % check n_bins and set default if not provided
    if ~isfield(opts,'n_binsX1')
        opts.n_binsX1 = 3;
    else
        assert(opts.n_binsX1 > 1,...
            'opts.n_binsX1 should be > 1');
    end
    if ~isfield(opts,'n_binsX2')
        opts.n_binsX2 = 3;
    else
        assert(opts.n_binsX2 > 1,...
            'opts.n_binsX2 should be > 1');
    end
    if ~isfield(opts,'n_binsY')
        opts.n_binsY = 3;
    else
        assert(opts.n_binsY > 1,...
            'opts.n_binsY should be > 1');
    end
    % check bootstrapping options
    if ~isfield(opts,'btsp')
        opts.btsp = 0;
        opts.btsp_variables = {};
        opts.btsp_type = {};
    else
        assert(opts.btsp > 0 && mod(opts.btsp,1) == 0,...
            'opts.btsp should be an integer >= 0')
    end
    if opts.btsp > 0
        assert(isfield(opts,'btsp_variables'),...
            'opts.btsp option requires opts.btsp_variables');
        assert(isfield(opts,'btsp_type'),...
            'opts.btsp option requires opts.btsp_type');
        assert(iscell(opts.btsp_variables),...
            'opts.btsp_variables option should be a cell array');
        assert(iscell(opts.btsp_type),...
            'opts.btsp_type option should be a cell array');
        assert(length(opts.btsp_variables) == length(opts.btsp_type),...
            'cell arrays opts.btsp_variables and opts.btsp_type should have the same size')
        for i = 1:length(opts.btsp_variables)
            assert(strcmp(opts.btsp_variables{i},'Y') ||...
                strcmp(opts.btsp_variables{i},'X1') ||...
                strcmp(opts.btsp_variables{i}, 'X2'),...
                ['Elements in opts.btsp_variables{:} should be one of ''X1'', ''X2'', or ''Y''. ',...
                'Specified value ''%s'''], opts.btsp_variables{i});
            assert(strcmp(opts.btsp_type{i},'all') ||...
                strcmp(opts.btsp_type{i},'X1conditioned') ||...
                strcmp(opts.btsp_type{i},'X2conditioned') ||...
                strcmp(opts.btsp_type{i},'Yconditioned'),...
                ['Elements in opts.btsp_type{:} should be one of ''all'', ''X1conditioned'', ''X2conditioned'', or ''Yconditioned''. ',...
                'Specified value ''%s'''], opts.btsp_type{i});
            splitstring = split(opts.btsp_type{i},'conditioned');
            conditioning_vars{i} = splitstring{1};
            assert(~strcmp(opts.btsp_variables{i},conditioning_vars{i}),...
                ['Shuffle on variable ''%s'' cannot be performed by conditioning on values of the same variable. ', ...
                'The operation has no meaning.'], opts.btsp_variables{i});
        end
    end
end

% set up split factors and other method-depending vars and
% check that number of draws per number of splits is compatible
% with the size of the data (though unlikely to be otherwise)
if strcmp(opts.bias, 'le')
    split_factors = [1 0.5];
    calculate_bias = true;
elseif strcmp(opts.bias, 'qe')
    split_factors = [1 0.5 0.25];
    calculate_bias = true;
else
    split_factors = [1];
    calculate_bias = false;
end

n_trials = length(X1(1,:));
n_splits = numel(split_factors);

for i=2:numel(split_factors)
    sf = split_factors(i);
    n_split_trials = floor(n_trials*sf);
    max_n_draws = round(opts.max_draws_per_split_number/sf);
	warning('off','MATLAB:nchoosek:LargeCoefficient')
    assert(max_n_draws <= nchoosek(n_trials,n_split_trials), ['Number of ',...
        'draws per split number ''opts.max_draws_per_split_number'' specified ',...
        'is higher than allowed given sample size. Max allowed for '...
        'method %s and split factor %g is: %i'],...
        opts.bin_methodX1, sf, floor(nchoosek(n_trials,n_split_trials)*sf));
end

% bin X if required
if ~strcmp(opts.bin_methodX1, 'none')
    X1_b = binr(X1, opts.n_binsX1, opts.bin_methodX1);
else
    X1_b = X1;
end
if ~strcmp(opts.bin_methodX2, 'none')
    X2_b = binr(X2, opts.n_binsX2, opts.bin_methodX2);
else
    X2_b = X2;
end
if ~strcmp(opts.bin_methodY, 'none')
    Y_b = binr(Y, opts.n_binsY, opts.bin_methodY);
else
    Y_b = Y;
end

% if multi-dimensional response map it onto a 1D space
if length(X1_b(:,1)) > 1
    X1_b = map_Nd_array_to_1d(X1_b);
end
if length(X2_b(:,1)) > 1
    X2_b = map_Nd_array_to_1d(X2_b);
end
if length(Y_b(:,1)) > 1
    Y_b = map_Nd_array_to_1d(Y_b);
end

% calculate PID on unshuffled data
if calculate_bias
    [PID.biased.shared.value,...
        PID.biased.uniqueX1.value,...
        PID.biased.uniqueX2.value,...
        PID.biased.complementary.value,...
        PID.unbiased.shared.value,...
        PID.unbiased.uniqueX1.value,...
        PID.unbiased.uniqueX2.value,...
        PID.unbiased.complementary.value] = calculate_PID(Y_b,X1_b,X2_b,...
        n_splits,split_factors,n_trials,opts);
else
    [PID.biased.shared.value,...
        PID.biased.uniqueX1.value,...
        PID.biased.uniqueX2.value,...
        PID.biased.complementary.value] = calculate_PID(Y_b,X1_b,X2_b,...
        n_splits,split_factors,n_trials,opts);
end

% Calculate PID on shuffled data if required
for s = 1:length(opts.btsp_variables)
    % initialize arrays for btsp results
    PID.biased.uniqueX1.btsp{s}                  = NaN(1,opts.btsp);
    PID.biased.uniqueX2.btsp{s}                  = NaN(1,opts.btsp);
    PID.biased.shared.btsp{s}                    = NaN(1,opts.btsp);
    PID.biased.complementary.btsp{s}             = NaN(1,opts.btsp);
    PID.unbiased.uniqueX1.btsp{s}                = NaN(1,opts.btsp);
    PID.unbiased.uniqueX2.btsp{s}                = NaN(1,opts.btsp);
    PID.unbiased.shared.btsp{s}                  = NaN(1,opts.btsp);
    PID.unbiased.complementary.btsp{s}           = NaN(1,opts.btsp);
    
    switch opts.btsp_variables{s}
        case 'Y'
            shuffled_var = Y_b;
        case 'X1'
            shuffled_var = X1_b;
        case 'X2'
            shuffled_var = X2_b;
    end
    switch conditioning_vars{s}
        case 'all'
            % un-conditioned case
            cond_var = [];
        case 'X1'
            % X1 conditioned
            cond_var = X1_b;
        case 'X2'
            % X2 conditioned
            cond_var = X2_b;
        case 'Y'
            % Y conditioned
            cond_var = Y_b;
    end
    
    for b = 1:opts.btsp
        if isempty(cond_var)
            shuffled_var = shuffled_var(randperm(length(shuffled_var)));
        else
            unique_vals = unique(cond_var);
            for i = 1:length(unique_vals)
                val_inds = find(cond_var == unique_vals(i));
                shuffled_val_inds = val_inds(randperm(length(val_inds)));
                shuffled_var(val_inds) = shuffled_var(shuffled_val_inds);
            end
        end
        if calculate_bias
            switch opts.btsp_variables{s}
                case 'Y'
                    [PID.biased.shared.btsp{s}(b),...
                        PID.biased.uniqueX1.btsp{s}(b),...
                        PID.biased.uniqueX2.btsp{s}(b),...
                        PID.biased.complementary.btsp{s}(b),...
                        PID.unbiased.shared.btsp{s}(b),...
                        PID.unbiased.uniqueX1.btsp{s}(b),...
                        PID.unbiased.uniqueX2.btsp{s}(b),...
                        PID.unbiased.complementary.btsp{s}(b)] = calculate_PID(...
                        shuffled_var,X1_b,X2_b,...
                        n_splits,split_factors,n_trials,opts);
                case 'X1'
                    [PID.biased.shared.btsp{s}(b),...
                        PID.biased.uniqueX1.btsp{s}(b),...
                        PID.biased.uniqueX2.btsp{s}(b),...
                        PID.biased.complementary.btsp{s}(b),...
                        PID.unbiased.shared.btsp{s}(b),...
                        PID.unbiased.uniqueX1.btsp{s}(b),...
                        PID.unbiased.uniqueX2.btsp{s}(b),...
                        PID.unbiased.complementary.btsp{s}(b)] = calculate_PID(...
                        Y_b,shuffled_var,X2_b,...
                        n_splits,split_factors,n_trials,opts);
                case 'X2'
                    [PID.biased.shared.btsp{s}(b),...
                        PID.biased.uniqueX1.btsp{s}(b),...
                        PID.biased.uniqueX2.btsp{s}(b),...
                        PID.biased.complementary.btsp{s}(b),...
                        PID.unbiased.shared.btsp{s}(b),...
                        PID.unbiased.uniqueX1.btsp{s}(b),...
                        PID.unbiased.uniqueX2.btsp{s}(b),...
                        PID.unbiased.complementary.btsp{s}(b)] = calculate_PID(...
                        Y_b,X1_b,shuffled_var,...
                        n_splits,split_factors,n_trials,opts);
            end
        else
            switch opts.btsp_variables{s}
                case 'Y'
                    [PID.biased.shared.btsp{s}(b),...
                        PID.biased.uniqueX1.btsp{s}(b),...
                        PID.biased.uniqueX2.btsp{s}(b),...
                        PID.biased.complementary.btsp{s}(b)] = calculate_PID(...
                        shuffled_var,X1_b,X2_b,...
                        n_splits,split_factors,n_trials,opts);
                case 'X1'
                    [PID.biased.shared.btsp{s}(b),...
                        PID.biased.uniqueX1.btsp{s}(b),...
                        PID.biased.uniqueX2.btsp{s}(b),...
                        PID.biased.complementary.btsp{s}(b)] = calculate_PID(...
                        Y_b,shuffled_var,X2_b,...
                        n_splits,split_factors,n_trials,opts);
                case 'X2'
                    [PID.biased.shared.btsp{s}(b),...
                        PID.biased.uniqueX1.btsp{s}(b),...
                        PID.biased.uniqueX2.btsp{s}(b),...
                        PID.biased.complementary.btsp{s}(b)] = calculate_PID(...
                        Y_b,X1_b,shuffled_var,...
                        n_splits,split_factors,n_trials,opts);
            end
        end
    end
end
end
%% Function that calculated PID atoms for a single shuffling
function [biased_shared, biased_uniqueX1, biased_uniqueX2,...
    biased_complementary, varargout] = calculate_PID(Y_b,X1_b,X2_b,n_splits,...
    split_factors,n_trials,opts)

PIDsi_splits = zeros(1,n_splits);   % shared
PIDuix1_splits = zeros(1,n_splits);	% unique X1
PIDuix2_splits = zeros(1,n_splits);	% unique X2
PIDci_splits = zeros(1,n_splits);	% complementary

if n_splits == 1
    parforArg = 0;
else
    parforArg = Inf;
end

for s = 1:n_splits
    sf = split_factors(s);
    n_split_trials(s) = floor(n_trials*sf);
    if sf == 1
        max_n_draws = 1;
    else
        max_n_draws = round(opts.max_draws_per_split_number/sf);
    end
    PIDsi_tmp = zeros(1,max_n_draws);
    PIDuix1_tmp = zeros(1,max_n_draws);
    PIDuix2_tmp = zeros(1,max_n_draws);
    PIDci_tmp = zeros(1,max_n_draws);
    
    parfor (d = 1:max_n_draws, parforArg)
        % fetch parallel results as they arrive
        PID_tmp = calculate_pid(Y_b,X1_b,X2_b,n_trials,n_split_trials(s));
        PIDsi_tmp(d) = PID_tmp(1);
        PIDuix1_tmp(d) = PID_tmp(2);
        PIDuix2_tmp(d) = PID_tmp(3);
        PIDci_tmp(d) = PID_tmp(4);
    end
    PIDsi_splits(s) = mean(PIDsi_tmp);
    PIDuix1_splits(s) = mean(PIDuix1_tmp);
    PIDuix2_splits(s) = mean(PIDuix2_tmp);
    PIDci_splits(s) = mean(PIDci_tmp);
end

% biased PID
biased_shared = PIDsi_splits(1);
biased_uniqueX1 = PIDuix1_splits(1);
biased_uniqueX2 = PIDuix2_splits(1);
biased_complementary = PIDci_splits(1);
% bias-corrected PID
if strcmp(opts.bias, 'le')
    p = polyfit(1./n_split_trials, PIDsi_splits, 1);
    varargout{1} = p(2);
    p = polyfit(1./n_split_trials, PIDuix1_splits, 1);
    varargout{2} = p(2);
    p = polyfit(1./n_split_trials, PIDuix2_splits, 1);
    varargout{3} = p(2);
    p = polyfit(1./n_split_trials, PIDci_splits, 1);
    varargout{4} = p(2);
elseif strcmp(opts.bias, 'qe')
    p = polyfit(1./n_split_trials, PIDsi_splits, 2);
    varargout{1} = p(3);
    p = polyfit(1./n_split_trials, PIDuix1_splits, 2);
    varargout{2} = p(3);
    p = polyfit(1./n_split_trials, PIDuix2_splits, 2);
    varargout{3} = p(3);
    p = polyfit(1./n_split_trials, PIDci_splits, 2);
    PID.unbiased.complementary.value = p(3);
    varargout{4} = p(3);
end
end