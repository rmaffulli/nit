function [II, PID_SR_C, PID_RC_S] = II(S, R, C, varargin)
%%% *function [II, PID_SR_C, PID_RC_S] = II(S, R, C, varargin)*
%%%
%%% ### Description
%%% This function computes the intersection information II, either naive or bias-corrected, given input data.
%%%
%%% ### Inputs:
%%% - *S*: must be an array of *nDimsS X nTrials* elements representing the discrete value of the stimulus presented in each trial.
%%% - *R*: must be an array of *nDimsR X nTrials* response matrix describing the response of each of the *nDims* dimensions for each trial.
%%% - *C*: must be an array of *nDimsC X nTrials* elements representing the discrete value of the choice made by the subject in each trial.
%%% - *opts*: options used to calculate II (see further notes).
%%%
%%% ### Outputs:
%%% - *II*: data structure containing naive, and bias corrected (if required) estimation of intersection information $II = min[SI(S:{R,C}),SI(C:{R,S})]$ for the input data. If calculated, the structure contains as well the btsp estimate. In case of multiple bootstrapping options (i.e. in case of `length(opts.btsp_variables) > 1`) multiple bootstrap estimates are returned. 
%%% - *PID_SR_C*: data structure containing naive, and bias corrected (if required) estimation of the PID decomposition of $(S;R)$ with target $C$. If calculated, the structure contains as well the btsp estimate. In case of multiple bootstrapping options (i.e. in case of `length(opts.btsp_variables) > 1`) multiple bootstrap estimates are returned. 
%%% - *PID_RC_S*: data structure containing naive, and bias corrected (if required) estimation of the PID decomposition of $(R;C)$ with target $S$. If calculated, the structure contains as well the btsp estimate. In case of multiple bootstrapping options (i.e. in case of `length(opts.btsp_variables) > 1`) multiple bootstrap estimates are returned. 
%%%
%%% ### Further notes:
%%% The *opts* structure can have the following fields:
%%%
%%% | field                                | description                                                                                                                                                                                                                                                                      | allowed values                                                                                                                                                                                                                                                             | default   |
%%% |--------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------|
%%% | opts.bias                            | specifies the bias correction method                                                                                                                                                                                                                                             | `'naive'` (no bias correction)<br>`'le'` (linear extrapolation)<br>`'qe'` (quadratic extrapolation)                                                                                                                                                                        | `'naive'` |
%%% | opts.max_draws_per_split_number      | specifies the maximum number of draws on which to calculate the unbiased estimate of II. E.g. `opts.max_draws_per_split_number = 10`, the II is calculated as average on 20 trials for 2 splits and 10 trials for 1 split. (ignored if opts.bias = 'naive')                      | int > 0                                                                                                                                                                                                                                                                    | 50        |
%%% | opts.bin_methodS                     | specifies the binning method for the stimulus                                                                                                                                                                                                                                    | `'none'` (no binning)<br>`'eqpop'` (evenly populated binning)<br>`'eqspace'`(evenly spaced binning)<br>`'ceqspace'`(centered evenly spaced binning)<br>`'gseqspace'`(gaussian evenly spaced binning)<br> See the documentation of [[binr|binr]] function for more details  | `'none'`  |
%%% | opts.bin_methodR                     | specifies the binning method for the activity                                                                                                                                                                                                                                    | `'none'` (no binning)<br>`'eqpop'` (evenly populated binning)<br>`'eqspace'`(evenly spaced binning)<br>`'ceqspace'`(centered evenly spaced binning)<br>`'gseqspace'`(gaussian evenly spaced binning)<br> See the documentation of [[binr|binr]] function for more details  | `'eqpop'` |
%%% | opts.bin_methodC                     | specifies the binning method for the behavior                                                                                                                                                                                                                                    | `'none'` (no binning)<br>`'eqpop'` (evenly populated binning)<br>`'eqspace'`(evenly spaced binning)<br>`'ceqspace'`(centered evenly spaced binning)<br>`'gseqspace'`(gaussian evenly spaced binning)<br> See the documentation of [[binr|binr]] function for more details  | `'none'`  |
%%% | opts.n_binsS                         | number of bins to be used to reduce the dimensionality of the stimulus                                                                                                                                                                                                           | int > 1                                                                                                                                                                                                                                                                    | 3         |
%%% | opts.n_binsR                         | number of bins to be used to reduce the dimensionality of the activity                                                                                                                                                                                                           | int > 1                                                                                                                                                                                                                                                                    | 3         |
%%% | opts.n_binsC                         | number of bins to be used to reduce the dimensionality of the choice                                                                                                                                                                                                             | int > 1                                                                                                                                                                                                                                                                    | 3         |
%%% | opts.btsp                            | number of bootstrap operations to perform for significance testing (those will be performed independently on each of the variables listed in `opts.btsp_variables`)                                                                                                              | int >= 0                                                                                                                                                                                                                                                                   | 0         |
%%% | opts.btsp_variables                  | list of variables to be bootstrapped, specified as a cell array of strings. If multiple bootstrapping operations are requested, `opts.btsp_variables` can contain multiple variables. In this case a `opts.btsp_type` option should be specified for each `opts.btsp_variables`  | cell array containing one (or more) strings (`"S"`, `"R"` or `"C"`) corresponding to all variables to be bootstrapped                                                                                                                                                      | N/A       |
%%% | opts.btsp_type                       | type of bootstrapping to be applied for each variable (`'all'` shuffles all values of the corresponding variable in `opts.btsp_variables` across trials, while `'$VAR$conditioned'` shuffles trials by conditioning on values of the variable specified in the substring `$VAR$`)| cell array of same size of opts.btsp_variables, each element contains the type of shuffling to be performed for the variable (either "all"` or `"Cconditioned"`, `"Rconditioned"`, or `"Sconditioned"`)                                                                    | N/A       |
%%% | opts.btsp_bias                       | bias correction method to be applied for each variable (same values than in opts.bias)                                                                                                                                                                                           | cell array of same size of opts.btsp_variables, each element contains the type of bias correction to be performed                                                                                                                                                          | `'naive'` |
%%%
%
%  This source code is part of:
%  NIT - Neuroscience Information Toolbox
%  Copyright (C) 2020  Roberto Maffulli, Miguel Angel Casal Santiago
%
%  This program is free software: you can redistribute it and/or modify
%  it under the terms of the GNU General Public License as published by
%  the Free Software Foundation, either version 3 of the License, or
%  (at your option) any later version.
%
%  This program is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%  GNU General Public License for more details.
% 
%  You should have received a copy of the GNU General Public License
%  along with this program.  If not, see <https://www.gnu.org/licenses/>.

% check inputs dimensions
assert(length(R(1,:)) == length(S(1,:)) && length(R(1,:)) == length(C(1,:)),...
    "Number of trials are differing in S, R and C");

% check for nans
if any(isnan(S))
    error("S contains NaNs. Aborting.")
end
if any(isnan(R))
    error("R contains NaNs. Aborting.")
end
if any(isnan(C))
    error("C contains NaNs. Aborting.")
end

% set default options
if nargin == 3
    opts.bias = 'naive';
    opts.bin_methodR = 'eqpop';
    opts.bin_methodS = 'none';
    opts.bin_methodC = 'none';
    opts.n_binsS = 3;
    opts.n_binsR = 3;
    opts.n_binsC = 3;
    opts.max_draws_per_split_number = 200;
    opts.nullhyp = 0;
    opts.nh_perm = 0;
else
    opts = varargin{1};
    % check bias reduction method and set default if not provided
    if ~isfield(opts,'bias')
        opts.bias = 'naive';
    else
        assert(strcmp(opts.bias,'naive') ||...
            strcmp(opts.bias, 'le') ||...
            strcmp(opts.bias, 'qe'),...
            ['opts.bias argument can be only ''naive'', ''le'' or ''qe''. ',...
            'Specified value ''%s'''], opts.bias);
    end
    % check draws_per_split_number and set default if not provided
    if ~isfield(opts,'max_draws_per_split_number')
        opts.max_draws_per_split_number = 50;
    else
        assert(opts.max_draws_per_split_number > 0,...
            'opts.max_draws_per_split_number should be > 0');
    end   
    % check binning method and set default if not provided
    if ~isfield(opts,'bin_methodR')
        opts.bin_methodR = 'none';
    else
        assert(strcmp(opts.bin_methodR,'none') ||...
            strcmp(opts.bin_methodR,'eqpop') ||...
            strcmp(opts.bin_methodR, 'eqspace') ||...
            strcmp(opts.bin_methodR, 'ceqspace') ||...
            strcmp(opts.bin_methodR, 'gseqspace'),...
            ['opts.bin_methodR argument can be only ''eqpop'', ''eqspace'', ''ceqspace'' or ''gseqspace''. ',...
            'Specified value ''%s'''], opts.bin_methodR);
    end
    if ~isfield(opts,'bin_methodS')
        opts.bin_methodS = 'none';
    else
        assert(strcmp(opts.bin_methodS,'none') ||...
            strcmp(opts.bin_methodS,'eqpop') ||...
            strcmp(opts.bin_methodS, 'eqspace') ||...
            strcmp(opts.bin_methodS, 'ceqspace') ||...
            strcmp(opts.bin_methodS, 'gseqspace'),...
            ['opts.bin_methodS argument can be only ''eqpop'', ''eqspace'', ''ceqspace'' or ''gseqspace''. ',...
            'Specified value ''%s'''], opts.bin_methodS);
    end
    if ~isfield(opts,'bin_methodC')
        opts.bin_methodC = 'none';
    else
        assert(strcmp(opts.bin_methodC,'none') ||...
            strcmp(opts.bin_methodC,'eqpop') ||...
            strcmp(opts.bin_methodC, 'eqspace') ||...
            strcmp(opts.bin_methodC, 'ceqspace') ||...
            strcmp(opts.bin_methodC, 'gseqspace'),...
            ['opts.bin_methodC argument can be only ''eqpop'', ''eqspace'', ''ceqspace'' or ''gseqspace''. ',...
            'Specified value ''%s'''], opts.bin_methodC);
    end
    % check n_bins and set default if not provided
    if ~isfield(opts,'n_binsS')
        opts.n_binsS = 3;
    else
        assert(opts.n_binsS > 1,...
            'opts.n_binsS should be > 1');
    end
    if ~isfield(opts,'n_binsR')
        opts.n_binsR = 3;
    else
        assert(opts.n_binsR > 1,...
            'opts.n_binsR should be > 1');
    end
    if ~isfield(opts,'n_binsC')
        opts.n_binsC = 3;
    else
        assert(opts.n_binsC > 1,...
            'opts.n_binsC should be > 1');
    end
    % check bootstrapping options
    if ~isfield(opts,'btsp')
        opts.btsp = 0;
        opts.btsp_variables = {};
        opts.btsp_type = {};
        opts.btsp_variables = {};
    else
        assert(opts.btsp > 0 && mod(opts.btsp,1) == 0,...
            'opts.btsp should be an integer >= 0')
    end
    if opts.btsp > 0
        assert(isfield(opts,'btsp_variables'),...
            'opts.btsp option requires opts.btsp_variables');
        assert(isfield(opts,'btsp_type'),...
            'opts.btsp option requires opts.btsp_type');
        assert(isfield(opts,'btsp_bias'),...
            'opts.btsp option requires opts.btsp_bias');
        assert(iscell(opts.btsp_variables),...
            'opts.btsp_variables option should be a cell array');
        assert(iscell(opts.btsp_type),...
            'opts.btsp_type option should be a cell array');
        assert(iscell(opts.btsp_bias),...
            'opts.btsp_bias option should be a cell array');
        assert(length(opts.btsp_variables) == length(opts.btsp_type),...
            'cell arrays opts.btsp_variables and opts.btsp_type should have the same size')
        assert(length(opts.btsp_variables) == length(opts.btsp_bias),...
            'cell arrays opts.btsp_variables and opts.btsp_bias should have the same size')
        assert(length(opts.btsp_bias) == length(opts.btsp_type),...
            'cell arrays opts.btsp_bias and opts.btsp_type should have the same size')
        for i = 1:length(opts.btsp_variables)
            assert(strcmp(opts.btsp_variables{i},'S') ||...
                strcmp(opts.btsp_variables{i},'R') ||...
                strcmp(opts.btsp_variables{i}, 'C'),...
                ['Elements in opts.btsp_variables{:} should be one of ''S'', ''R'', or ''C''. ',...
                'Specified value ''%s'''], opts.btsp_variables{i});
            assert(strcmp(opts.btsp_type{i},'all') ||...
                strcmp(opts.btsp_type{i},'Sconditioned') ||...
                strcmp(opts.btsp_type{i},'Rconditioned') ||...
                strcmp(opts.btsp_type{i},'Cconditioned'),...
                ['Elements in opts.btsp_type{:} should be one of ''all'', ''Sconditioned'', ''Rconditioned'', or ''Cconditioned''. ',...
                'Specified value ''%s'''], opts.btsp_type{i});
            assert(strcmp(opts.btsp_bias{i},'naive') ||...
                strcmp(opts.btsp_bias{i},'qe') ||...
                strcmp(opts.btsp_bias{i}, 'le'),...
                ['Elements in opts.btsp_variables{:} should be one of ''naive'', ''le'', or ''qe''. ',...
                'Specified value ''%s'''], opts.btsp_bias{i});
            splitstring = split(opts.btsp_type{i},'conditioned');
            conditioning_vars{i} = splitstring{1};
            assert(~strcmp(opts.btsp_variables{i},conditioning_vars{i}),...
                ['Shuffle on variable ''%s'' cannot be performed by conditioning on values of the same variable. ', ...
                'The operation has no meaning.'], opts.btsp_variables{i});
        end
    end
end

% set up split factors and other method-depending vars and
% check that number of draws per number of splits is compatible
% with the size of the data (though unlikely to be otherwise)
if strcmp(opts.bias, 'le')
    split_factors = [1 0.5];
    calculate_bias = true;
elseif strcmp(opts.bias, 'qe')
    split_factors = [1 0.5 0.25];
    calculate_bias = true;
else
    split_factors = [1];
    calculate_bias = false;
end

n_trials = length(S(1,:));
n_splits = numel(split_factors);

for i=2:numel(split_factors)
    sf = split_factors(i);
    n_split_trials = floor(n_trials*sf);
    max_n_draws = round(opts.max_draws_per_split_number/sf);
	warning('off','MATLAB:nchoosek:LargeCoefficient')
    assert(max_n_draws <= nchoosek(n_trials,n_split_trials), ['Number of ',...
        'draws per split number ''opts.max_draws_per_split_number'' specified ',...
        'is higher than allowed given sample size. Max allowed for '...
        'method %s and split factor %g is: %i'],...
        opts.bin_methodR, sf, floor(nchoosek(n_trials,n_split_trials)*sf));
end

% bin R if required
if ~strcmp(opts.bin_methodR, 'none')
    R_b = binr(R, opts.n_binsR, opts.bin_methodR);
else
    R_b = R;
end
% bin S if required
if ~strcmp(opts.bin_methodS, 'none')
    S_b = binr(S, opts.n_binsS, opts.bin_methodS);
else
    S_b = S;
end
% bin C if required
if ~strcmp(opts.bin_methodC, 'none')
    C_b = binr(C, opts.n_binsC, opts.bin_methodC);
else
    C_b = C;
end

% if multi-dimensional response map it onto a 1D space
if length(R_b(:,1)) > 1
    R_b = map_Nd_array_to_1d(R_b);
end
if length(S_b(:,1)) > 1
    S_b = map_Nd_array_to_1d(S_b);
end
if length(C_b(:,1)) > 1
    C_b = map_Nd_array_to_1d(C_b);
end

% calculate II
if calculate_bias
    [II.biased.value,...
        PID_RC_S.biased.shared.value,...
        PID_RC_S.biased.uniqueR.value,...
        PID_RC_S.biased.uniqueC.value,...
        PID_RC_S.biased.complementary.value,...
        PID_SR_C.biased.shared.value,...
        PID_SR_C.biased.uniqueR.value,...
        PID_SR_C.biased.uniqueS.value,...
        PID_SR_C.biased.complementary.value,...
        II.unbiased.value,...
        PID_RC_S.unbiased.shared.value,...
        PID_RC_S.unbiased.uniqueR.value,...
        PID_RC_S.unbiased.uniqueC.value,...
        PID_RC_S.unbiased.complementary.value,...
        PID_SR_C.unbiased.shared.value,...
        PID_SR_C.unbiased.uniqueR.value,...
        PID_SR_C.unbiased.uniqueS.value,...
        PID_SR_C.unbiased.complementary.value] =...
        calculate_bias_corrected_II(S_b,R_b,C_b,...
        n_splits,split_factors,n_trials,opts,opts.bias);
else
    [II.biased.value,...
        PID_RC_S.biased.shared.value,...
        PID_RC_S.biased.uniqueR.value,...
        PID_RC_S.biased.uniqueC.value,...
        PID_RC_S.biased.complementary.value,...
        PID_SR_C.biased.shared.value,...
        PID_SR_C.biased.uniqueR.value,...
        PID_SR_C.biased.uniqueS.value,...
        PID_SR_C.biased.complementary.value] =...
        calculate_bias_corrected_II(S_b,R_b,C_b,...
        n_splits,split_factors,n_trials,opts,opts.bias);
end

% Calculate II on shuffled data if required
for s = 1:length(opts.btsp_variables)
    % initialize arrays for btsp results
    II.biased.btsp{s}                       = NaN(1,opts.btsp);
    PID_RC_S.biased.shared.btsp{s}          = NaN(1,opts.btsp);
    PID_RC_S.biased.uniqueR.btsp{s}         = NaN(1,opts.btsp);
    PID_RC_S.biased.uniqueC.btsp{s}         = NaN(1,opts.btsp);
    PID_RC_S.biased.complementary.btsp{s}   = NaN(1,opts.btsp);
    PID_SR_C.biased.shared.btsp{s}          = NaN(1,opts.btsp);
    PID_SR_C.biased.uniqueR.btsp{s}         = NaN(1,opts.btsp);
    PID_SR_C.biased.uniqueS.btsp{s}         = NaN(1,opts.btsp);
    PID_SR_C.biased.complementary.btsp{s}   = NaN(1,opts.btsp);
    II.unbiased.btsp{s}                     = NaN(1,opts.btsp);
    PID_RC_S.unbiased.shared.btsp{s}        = NaN(1,opts.btsp);
    PID_RC_S.unbiased.uniqueR.btsp{s}       = NaN(1,opts.btsp);
    PID_RC_S.unbiased.uniqueC.btsp{s}       = NaN(1,opts.btsp);
    PID_RC_S.unbiased.complementary.btsp{s} = NaN(1,opts.btsp);
    PID_SR_C.unbiased.shared.btsp{s}        = NaN(1,opts.btsp);
    PID_SR_C.unbiased.uniqueR.btsp{s}       = NaN(1,opts.btsp);
    PID_SR_C.unbiased.uniqueS.btsp{s}       = NaN(1,opts.btsp);
    PID_SR_C.unbiased.complementary.btsp{s} = NaN(1,opts.btsp);
    
    % define variables to shuffle based on user inputs
    switch opts.btsp_variables{s}
        case 'S'
            shuffled_var = S_b;
        case 'R'
            shuffled_var = R_b;
        case 'C'
            shuffled_var = C_b;
    end
    switch conditioning_vars{s}
        case 'all'
            % un-conditioned case
            cond_var = [];
        case 'S'
            % S conditioned
            cond_var = S_b;
        case 'R'
            % R conditioned
            cond_var = R_b;
        case 'C'
            % C conditioned
            cond_var = C_b;
    end
    
    if strcmp(opts.btsp_bias{s}, 'le')
        split_factors_btsp = [1 0.5];
        calculate_bias_btsp = true;
    elseif strcmp(opts.btsp_bias{s}, 'qe')
        split_factors_btsp = [1 0.5 0.25];
        calculate_bias_btsp = true;
    else
        split_factors_btsp = [1];
        calculate_bias_btsp = false;
    end
    n_splits_btsp = numel(split_factors_btsp);

    for b = 1:opts.btsp
        if isempty(cond_var)
            shuffled_var = shuffled_var(randperm(length(shuffled_var)));
        else
            unique_vals = unique(cond_var);
            for i = 1:length(unique_vals)
                val_inds = find(cond_var == unique_vals(i));
                shuffled_val_inds = val_inds(randperm(length(val_inds)));
                shuffled_var(val_inds) = shuffled_var(shuffled_val_inds);
            end
        end
        if calculate_bias_btsp
            switch opts.btsp_variables{s}
                case 'S'
                    [II.biased.btsp{s}(b),...
                        PID_RC_S.biased.shared.btsp{s}(b),...
                        PID_RC_S.biased.uniqueR.btsp{s}(b),...
                        PID_RC_S.biased.uniqueC.btsp{s}(b),...
                        PID_RC_S.biased.complementary.btsp{s}(b),...
                        PID_SR_C.biased.shared.btsp{s}(b),...
                        PID_SR_C.biased.uniqueR.btsp{s}(b),...
                        PID_SR_C.biased.uniqueS.btsp{s}(b),...
                        PID_SR_C.biased.complementary.btsp{s}(b),...
                        II.unbiased.btsp{s}(b),...
                        PID_RC_S.unbiased.shared.btsp{s}(b),...
                        PID_RC_S.unbiased.uniqueR.btsp{s}(b),...
                        PID_RC_S.unbiased.uniqueC.btsp{s}(b),...
                        PID_RC_S.unbiased.complementary.btsp{s}(b),...
                        PID_SR_C.unbiased.shared.btsp{s}(b),...
                        PID_SR_C.unbiased.uniqueR.btsp{s}(b),...
                        PID_SR_C.unbiased.uniqueS.btsp{s}(b),...
                        PID_SR_C.unbiased.complementary.btsp{s}(b)] = ...
                        calculate_bias_corrected_II(shuffled_var,R_b,C_b,...
                        n_splits_btsp,split_factors_btsp,n_trials,opts,opts.btsp_bias{s});
                case 'R'
                    [II.biased.btsp{s}(b),...
                        PID_RC_S.biased.shared.btsp{s}(b),...
                        PID_RC_S.biased.uniqueR.btsp{s}(b),...
                        PID_RC_S.biased.uniqueC.btsp{s}(b),...
                        PID_RC_S.biased.complementary.btsp{s}(b),...
                        PID_SR_C.biased.shared.btsp{s}(b),...
                        PID_SR_C.biased.uniqueR.btsp{s}(b),...
                        PID_SR_C.biased.uniqueS.btsp{s}(b),...
                        PID_SR_C.biased.complementary.btsp{s}(b),...
                        II.unbiased.btsp{s}(b),...
                        PID_RC_S.unbiased.shared.btsp{s}(b),...
                        PID_RC_S.unbiased.uniqueR.btsp{s}(b),...
                        PID_RC_S.unbiased.uniqueC.btsp{s}(b),...
                        PID_RC_S.unbiased.complementary.btsp{s}(b),...
                        PID_SR_C.unbiased.shared.btsp{s}(b),...
                        PID_SR_C.unbiased.uniqueR.btsp{s}(b),...
                        PID_SR_C.unbiased.uniqueS.btsp{s}(b),...
                        PID_SR_C.unbiased.complementary.btsp{s}(b)] = ...
                        calculate_bias_corrected_II(S_b,shuffled_var,C_b,...
                        n_splits_btsp,split_factors_btsp,n_trials,opts,opts.btsp_bias{s});
                case 'C'
                    [II.biased.btsp{s}(b),...
                        PID_RC_S.biased.shared.btsp{s}(b),...
                        PID_RC_S.biased.uniqueR.btsp{s}(b),...
                        PID_RC_S.biased.uniqueC.btsp{s}(b),...
                        PID_RC_S.biased.complementary.btsp{s}(b),...
                        PID_SR_C.biased.shared.btsp{s}(b),...
                        PID_SR_C.biased.uniqueR.btsp{s}(b),...
                        PID_SR_C.biased.uniqueS.btsp{s}(b),...
                        PID_SR_C.biased.complementary.btsp{s}(b),...
                        II.unbiased.btsp{s}(b),...
                        PID_RC_S.unbiased.shared.btsp{s}(b),...
                        PID_RC_S.unbiased.uniqueR.btsp{s}(b),...
                        PID_RC_S.unbiased.uniqueC.btsp{s}(b),...
                        PID_RC_S.unbiased.complementary.btsp{s}(b),...
                        PID_SR_C.unbiased.shared.btsp{s}(b),...
                        PID_SR_C.unbiased.uniqueR.btsp{s}(b),...
                        PID_SR_C.unbiased.uniqueS.btsp{s}(b),...
                        PID_SR_C.unbiased.complementary.btsp{s}(b)] = ...
                        calculate_bias_corrected_II(S_b,R_b,shuffled_var,...
                        n_splits_btsp,split_factors_btsp,n_trials,opts,opts.btsp_bias{s});
            end
        else
            switch opts.btsp_variables{s}
                case 'S'
                    [II.biased.btsp{s}(b),...
                        PID_RC_S.biased.shared.btsp{s}(b),...
                        PID_RC_S.biased.uniqueR.btsp{s}(b),...
                        PID_RC_S.biased.uniqueC.btsp{s}(b),...
                        PID_RC_S.biased.complementary.btsp{s}(b),...
                        PID_SR_C.biased.shared.btsp{s}(b),...
                        PID_SR_C.biased.uniqueR.btsp{s}(b),...
                        PID_SR_C.biased.uniqueS.btsp{s}(b),...
                        PID_SR_C.biased.complementary.btsp{s}(b)] = ...
                        calculate_bias_corrected_II(shuffled_var,R_b,C_b,...
                        n_splits_btsp,split_factors_btsp,n_trials,opts,opts.btsp_bias{s});
                case 'R'
                    [II.biased.btsp{s}(b),...
                        PID_RC_S.biased.shared.btsp{s}(b),...
                        PID_RC_S.biased.uniqueR.btsp{s}(b),...
                        PID_RC_S.biased.uniqueC.btsp{s}(b),...
                        PID_RC_S.biased.complementary.btsp{s}(b),...
                        PID_SR_C.biased.shared.btsp{s}(b),...
                        PID_SR_C.biased.uniqueR.btsp{s}(b),...
                        PID_SR_C.biased.uniqueS.btsp{s}(b),...
                        PID_SR_C.biased.complementary.btsp{s}(b)] = ...
                        calculate_bias_corrected_II(S_b,shuffled_var,C_b,...
                        n_splits_btsp,split_factors_btsp,n_trials,opts,opts.btsp_bias{s});
                case 'C'
                    [II.biased.btsp{s}(b),...
                        PID_RC_S.biased.shared.btsp{s}(b),...
                        PID_RC_S.biased.uniqueR.btsp{s}(b),...
                        PID_RC_S.biased.uniqueC.btsp{s}(b),...
                        PID_RC_S.biased.complementary.btsp{s}(b),...
                        PID_SR_C.biased.shared.btsp{s}(b),...
                        PID_SR_C.biased.uniqueR.btsp{s}(b),...
                        PID_SR_C.biased.uniqueS.btsp{s}(b),...
                        PID_SR_C.biased.complementary.btsp{s}(b)] = ...
                        calculate_bias_corrected_II(S_b,R_b,shuffled_var,...
                        n_splits_btsp,split_factors_btsp,n_trials,opts,opts.btsp_bias{s});
            end
        end
    end
end
end

%% Helper functions
function [II_biased, PID_RC_S_sh_biased, PID_RC_S_ur_biased,...
    PID_RC_S_uc_biased, PID_RC_S_co_biased, PID_RS_C_sh_biased,...
    PID_RS_C_ur_biased, PID_RS_C_us_biased, PID_RS_C_co_biased,...
    varargout] = calculate_bias_corrected_II(S_b,R_b,C_b,...
    n_splits,split_factors,n_trials,opts,bias_method)

II_splits = zeros(1,n_splits);
PID_RC_S_sh_splits = zeros(1,n_splits);	% shared
PID_RC_S_ur_splits = zeros(1,n_splits);	% unique R
PID_RC_S_uc_splits = zeros(1,n_splits);	% unique C
PID_RC_S_co_splits = zeros(1,n_splits); % complementary
PID_RS_C_sh_splits = zeros(1,n_splits); % shared
PID_RS_C_ur_splits = zeros(1,n_splits); % unique R
PID_RS_C_us_splits = zeros(1,n_splits); % unique S
PID_RS_C_co_splits = zeros(1,n_splits); % complementary

if n_splits == 1
    parforArg = 0;
else
    parforArg = Inf;
end

for s = 1:n_splits
    sf = split_factors(s);
    n_split_trials(s) = floor(n_trials*sf);
    if sf == 1
        max_n_draws = 1;
    else
        max_n_draws = round(opts.max_draws_per_split_number/sf);
    end
    
    II_tmp = zeros(1,max_n_draws);
    PID_RC_S_sh_tmp = zeros(1,max_n_draws);
    PID_RC_S_ur_tmp = zeros(1,max_n_draws);
    PID_RC_S_uc_tmp = zeros(1,max_n_draws);
    PID_RC_S_co_tmp = zeros(1,max_n_draws);
    
    PID_RS_C_sh_tmp = zeros(1,max_n_draws);
    PID_RS_C_ur_tmp = zeros(1,max_n_draws);
    PID_RS_C_us_tmp = zeros(1,max_n_draws);
    PID_RS_C_co_tmp = zeros(1,max_n_draws);
    
    parfor (d = 1:max_n_draws, parforArg)
        % fetch parallel results as they arrive
        II_out = calculate_ii(S_b',C_b',R_b,n_trials,n_split_trials(s));
        
        II_tmp(d) = II_out(1);
        PID_RC_S_sh_tmp(d) = II_out(2);
        PID_RC_S_ur_tmp(d) = II_out(3);
        PID_RC_S_uc_tmp(d) = II_out(4);
        PID_RC_S_co_tmp(d) = II_out(5);
        
        PID_RS_C_sh_tmp(d) = II_out(6);
        PID_RS_C_ur_tmp(d) = II_out(7);
        PID_RS_C_us_tmp(d) = II_out(8);
        PID_RS_C_co_tmp(d) = II_out(9);
    end
    II_splits(s) = mean(II_tmp);
    PID_RC_S_sh_splits(s) = mean(PID_RC_S_sh_tmp);
    PID_RC_S_ur_splits(s) = mean(PID_RC_S_ur_tmp);
    PID_RC_S_uc_splits(s) = mean(PID_RC_S_uc_tmp);
    PID_RC_S_co_splits(s) = mean(PID_RC_S_co_tmp);
    PID_RS_C_sh_splits(s) = mean(PID_RS_C_sh_tmp);
    PID_RS_C_ur_splits(s) = mean(PID_RS_C_ur_tmp);
    PID_RS_C_us_splits(s) = mean(PID_RS_C_us_tmp);
    PID_RS_C_co_splits(s) = mean(PID_RS_C_co_tmp);
end

% biased quantities
II_biased = II_splits(1);
PID_RC_S_sh_biased = PID_RC_S_sh_splits(1);
PID_RC_S_ur_biased = PID_RC_S_ur_splits(1);
PID_RC_S_uc_biased = PID_RC_S_uc_splits(1);
PID_RC_S_co_biased = PID_RC_S_co_splits(1);
PID_RS_C_sh_biased = PID_RS_C_sh_splits(1);
PID_RS_C_ur_biased = PID_RS_C_ur_splits(1);
PID_RS_C_us_biased = PID_RS_C_us_splits(1);
PID_RS_C_co_biased = PID_RS_C_co_splits(1);


% bias-corrected II and PID quantities
if strcmp(bias_method, 'le')
    p = polyfit(1./n_split_trials, II_splits, 1);
    varargout{1} = p(2);
    p = polyfit(1./n_split_trials, PID_RC_S_sh_splits, 1);
    varargout{2} = p(2);
    p = polyfit(1./n_split_trials, PID_RC_S_ur_splits, 1);
    varargout{3} = p(2);
    p = polyfit(1./n_split_trials, PID_RC_S_uc_splits, 1);
    varargout{4} = p(2);
    p = polyfit(1./n_split_trials, PID_RC_S_co_splits, 1);
    varargout{5} = p(2);
    p = polyfit(1./n_split_trials, PID_RS_C_sh_splits, 1);
    varargout{6} = p(2);
    p = polyfit(1./n_split_trials, PID_RS_C_ur_splits, 1);
    varargout{7} = p(2);
    p = polyfit(1./n_split_trials, PID_RS_C_us_splits, 1);
    varargout{8} = p(2);
    p = polyfit(1./n_split_trials, PID_RS_C_co_splits, 1);
    varargout{9} = p(2);
elseif strcmp(bias_method, 'qe')
    p = polyfit(1./n_split_trials, II_splits, 2);
    varargout{1} = p(3);
    p = polyfit(1./n_split_trials, PID_RC_S_sh_splits, 2);
    varargout{2} = p(3);
    p = polyfit(1./n_split_trials, PID_RC_S_ur_splits, 2);
    varargout{3} = p(3);
    p = polyfit(1./n_split_trials, PID_RC_S_uc_splits, 2);
    varargout{4} = p(3);
    p = polyfit(1./n_split_trials, PID_RC_S_co_splits, 2);
    varargout{5} = p(3);
    p = polyfit(1./n_split_trials, PID_RS_C_sh_splits, 2);
    varargout{6} = p(3);
    p = polyfit(1./n_split_trials, PID_RS_C_ur_splits, 2);
    varargout{7} = p(3);
    p = polyfit(1./n_split_trials, PID_RS_C_us_splits, 2);
    varargout{8} = p(3);
    p = polyfit(1./n_split_trials, PID_RS_C_co_splits, 2);
    varargout{9} = p(3);
end
end
