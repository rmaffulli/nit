function [cFIT] = cFIT(S, hX, hY, Y, hZ, varargin)
%%% *function [cFIT_biased, varargout] = cFIT(S, hX, hY, Y, hZ, varargin)*
%%%
%%% ### Description
%%% This function computes the $Z$-conditioned Feature-specific (i.e. $S$-specific) Information Transmission (FIT) between a *source* $X$ and a *receiver* $Y$. NIT provides tools for both naive or bias-corrected estimation, given input data.
%%%
%%% ### Inputs:
%%% - *S*: must be an array of *nDimsS X nTrials * elements representing the discrete value of the stimulus presented in each trial.
%%% - *hX*: must be an array of *nDimsX X nTrials* response matrix describing the past of the emitter variables on each of the *nDims* dimensions for each trial.
%%% - *hY*: must be an array of *nDimsY X nTrials* response matrix describing the past of the receiver variable on each of the *nDims* dimensions for each trial.
%%% - *Y*: must be an array of *nDimsY X nTrials* response matrix describing the present of the emitter variable on each of the *nDims* dimensions for each trial.
%%% - *hZ*: must be an array of *nDimsZ X nTrials* response matrix describing the past of the confounding variable on each of the *nDims* dimensions for each trial.
%%% - *opts*: options used to calculate cFIT (see further notes).
%%%
%%% ### Outputs:
%%% - *cFIT_biased*: naive, biased estimation of conditional feature-specific information transmission for the input data.
%%% - *cFIT_unbiased*: unbiased estimate of feature-specific information transmission.  This is returned as second argument only if `opts.bias` is not `'naive'`.
%%% - *cFIT_pval*: p-value of the measure tested against the null hypothesis of Y being conditionally independent from X given S
%%% ### Further notes:
%%% The *opts* structure can have the following fields:
%%%
%%% | field                                | description                                                                                                                                                                                                                                             | allowed values                                                                                                                                                                                                                                                               | default   |
%%% |--------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------|
%%% | opts.bias                            | specifies the bias correction method                                                                                                                                                                                                                    | `'naive'` (no bias correction)<br>`'le'` (linear extrapolation)<br>`'qe'` (quadratic extrapolation)                                                                                                                                                                          | `'naive'` |
%%% | opts.max_draws_per_split_number      | specifies the maximum number of draws on which to calculate the unbiased estimate of II. E.g. `opts.max_draws_per_split_number = 10`, the II is calculated as average on 20 trials for 2 splits and 10 trials for 1 split. (ignored if opts.bias = 'naive') | int > 0                                                                                                                                                                                                                                                                  | 50        |
%%% | opts.bin_methodX                     | specifies the binning method for the X (emitter) signals                                                                                                                                                                                                | `'none'` (no binning)<br>`'eqpop'` (evenly populated binning)<br>`'eqspace'`(evenly spaced binning)<br>`'ceqspace'`(centered evenly spaced binning)<br>`'gseqspace'`(gaussian evenly spaced binning)<br> See the documentation of [[binr|binr]] function for more details    | `'eqpop'` |
%%% | opts.bin_methodY                     | specifies the binning method for the Y (receiver) signals                                                                                                                                                                                               | `'none'` (no binning)<br>`'eqpop'` (evenly populated binning)<br>`'eqspace'`(evenly spaced binning)<br>`'ceqspace'`(centered evenly spaced binning)<br>`'gseqspace'`(gaussian evenly spaced binning)<br> See the documentation of [[binr|binr]] function for more details    | `'eqpop'` |
%%% | opts.bin_methodZ                     | specifies the binning method for the Z (confounding) signals                                                                                                                                                                                            | `'none'` (no binning)<br>`'eqpop'` (evenly populated binning)<br>`'eqspace'`(evenly spaced binning)<br>`'ceqspace'`(centered evenly spaced binning)<br>`'gseqspace'`(gaussian evenly spaced binning)<br> See the documentation of [[binr|binr]] function for more details    | `'eqpop'` |
%%% | opts.bin_methodS                     | specifies the binning method for the stimulus signals                                                                                                                                                                                                   | `'none'` (no binning)<br>`'eqpop'` (evenly populated binning)<br>`'eqspace'`(evenly spaced binning)<br>`'ceqspace'`(centered evenly spaced binning)<br>`'gseqspace'`(gaussian evenly spaced binning)<br> See the documentation of [[binr|binr]] function for more details    | `'none'`  |
%%% | opts.n_binsX                         | number of bins to be used to reduce the dimensionality of the response X                                                                                                                                                                                | int > 1                                                                                                                                                                                                                                                                      | 3         |
%%% | opts.n_binsY                         | number of bins to be used to reduce the dimensionality of the response Y                                                                                                                                                                                | int > 1                                                                                                                                                                                                                                                                      | 3         |
%%% | opts.n_binsZ                         | number of bins to be used to reduce the dimensionality of the response Y                                                                                                                                                                                | int > 1                                                                                                                                                                                                                                                                      | 3         |
%%% | opts.n_binsS                         | number of bins to be used to reduce the dimensionality of the stimulus S                                                                                                                                                                                | int > 1                                                                                                                                                                                                                                                                      | 2         |
%%% | opts.nullhyp                         | If 'true' test the FIT value against the null hypothesis of Y being conditionally independent from X once S is given, via a permutation test. Otherwise, don't test significance                                                                        | bool                                                                                                                                                                                                                                                                         | false     |
%%% | opts.nh_perm                         | number of permutations used to build the null hypothesis distribution, by default 200                                                                                                                                                                   | int > 1                                                                                                                                                                                                                                                                      | 0         |
%
%  This source code is part of:
%  NIT - Neuroscience Information Toolbox
%  Copyright (C) 2020  Roberto Maffulli, Miguel Angel Casal Santiago, 
%  Marco Celotto
%
%  This program is free software: you can redistribute it and/or modify
%  it under the terms of the GNU General Public License as published by
%  the Free Software Foundation, either version 3 of the License, or
%  (at your option) any later version.
%
%  This program is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%  GNU General Public License for more details.
% 
%  You should have received a copy of the GNU General Public License
%  along with this program.  If not, see <https://www.gnu.org/licenses/>.

% check inputs dimensions
assert( length(S(1,:)) == length(hX(1,:)) || ...
     length(S(1,:)) == length(hY(1,:)) || ...
     length(S(1,:)) == length(hY(1,:)), ...
     'Number of trials are differing among the inputs')
    
assert(length(hX(1,:)) == length(S(1,:)),...
    "Number of columns of hX, hX(1,:), should be of size nTrials");
assert(length(hY(1,:)) == length(S(1,:)),...
    "Number of columns of hY, hY(1,:), should be of size nTrials");
assert(length(Y(1,:)) == length(S(1,:)),...
    "Number of columns of Y, Y(1,:), should be of size nTrials");
assert(length(hZ(1,:)) == length(S(1,:)),...
    "Number of columns of hZ, hZ(1,:), should be of size nTrials");
assert(length(hY(:,1)) == length(Y(:,1)), ...
    'Dimensions must match in hY and Y');

% check for nans
if any(isnan(S))
    error("S contains NaNs. Aborting.")
end
if any(isnan(hX))
    error("hX contains NaNs. Aborting.")
end
if any(isnan(hY))
    error("hY contains NaNs. Aborting.")
end
if any(isnan(Y))
    error("Y contains NaNs. Aborting.")
end
if any(isnan(hZ))
    error("hZ contains NaNs. Aborting.")
end

% set default options
if nargin == 5
    opts.bias = 'naive';
    opts.bin_methodX = 'eqpop';
    opts.bin_methodY = 'eqpop';
    opts.bin_methodZ = 'eqpop';
    opts.bin_methodS = 'none';
    opts.n_binsX = 3;
    opts.n_binsY = 3;
    opts.n_binsZ = 3;
    opts.n_binsS = 2;
    opts.max_draws_per_split_number = 200;
    opts.btsp = 0;
    opts.btsp_variables = {};
    opts.btsp_type = {};
else
    opts = varargin{1};
    % check bias reduction method and set default if not provided
    if ~isfield(opts,'bias')
        opts.bias = 'naive';
    else
        assert(strcmp(opts.bias,'naive') ||...
            strcmp(opts.bias, 'le') ||...
            strcmp(opts.bias, 'qe'),...
            ['opts.bias argument can be only ''naive'', ''le'' or ''qe''. ',...
            'Specified value ''%s'''], opts.bias);
    end
    % check draws_per_split_number and set default if not provided
    if ~isfield(opts,'max_draws_per_split_number')
        opts.max_draws_per_split_number = 50;
    else
        assert(opts.max_draws_per_split_number > 0,...
            'opts.max_draws_per_split_number should be > 0');
    end
    % check binning method and set default if not provided
    if ~isfield(opts,'bin_methodX')
        opts.bin_methodX = 'eqpop';
    else
        assert(strcmp(opts.bin_methodX,'none') ||...
            strcmp(opts.bin_methodX,'eqpop') ||...
            strcmp(opts.bin_methodX, 'eqspace') ||...
            strcmp(opts.bin_methodX, 'ceqspace') ||...
            strcmp(opts.bin_methodX, 'gseqspace'),...
            ['opts.bin_methodX argument can be only ''eqpop'', ''eqspace'', ''ceqspace'' or ''gseqspace''. ',...
            'Specified value ''%s'''], opts.bin_methodX);
    end
    if ~isfield(opts,'bin_methodY')
        opts.bin_methodY = 'eqpop';
    else
        assert(strcmp(opts.bin_methodY,'none') ||...
            strcmp(opts.bin_methodY,'eqpop') ||...
            strcmp(opts.bin_methodY, 'eqspace') ||...
            strcmp(opts.bin_methodY, 'ceqspace') ||...
            strcmp(opts.bin_methodY, 'gseqspace'),...
            ['opts.bin_methodY argument can be only ''eqpop'', ''eqspace'', ''ceqspace'' or ''gseqspace''. ',...
            'Specified value ''%s'''], opts.bin_methodY);
    end
    if ~isfield(opts,'bin_methodZ')
        opts.bin_methodZ = 'eqpop';
    else
        assert(strcmp(opts.bin_methodZ,'none') ||...
            strcmp(opts.bin_methodZ,'eqpop') ||...
            strcmp(opts.bin_methodZ, 'eqspace') ||...
            strcmp(opts.bin_methodZ, 'ceqspace') ||...
            strcmp(opts.bin_methodZ, 'gseqspace'),...
            ['opts.bin_methodZ argument can be only ''eqpop'', ''eqspace'', ''ceqspace'' or ''gseqspace''. ',...
            'Specified value ''%s'''], opts.bin_methodZ);
    end
    if ~isfield(opts,'bin_methodS')
        opts.bin_methodS = 'none';
    else
        assert(strcmp(opts.bin_methodS,'none') ||...
            strcmp(opts.bin_methodS,'eqpop') ||...
            strcmp(opts.bin_methodS, 'eqspace') ||...
            strcmp(opts.bin_methodS, 'ceqspace') ||...
            strcmp(opts.bin_methodS, 'gseqspace'),...
            ['opts.bin_methodS argument can be only ''eqpop'', ''eqspace'', ''ceqspace'' or ''gseqspace''. ',...
            'Specified value ''%s'''], opts.bin_methodS);
    end

    % check n_bins and set default if not provided
    if ~isfield(opts,'n_binsX')
        opts.n_binsX = 3;
    else
        assert(opts.n_binsX > 1,...
            'opts.n_binsX should be > 1');
    end 
    if ~isfield(opts,'n_binsY')
        opts.n_binsY = 3;
    else
        assert(opts.n_binsY > 1,...
            'opts.n_binsY should be > 1');
    end 
    if ~isfield(opts,'n_binsZ')
        opts.n_binsZ = 3;
    else
        assert(opts.n_binsZ > 1,...
            'opts.n_binsZ should be > 1');
    end  
    if ~isfield(opts,'n_binsS')
        opts.n_binsS = 2;
    else
        assert(opts.n_binsS > 1,...
            'opts.n_binsS should be > 1');
    end   
    
    % check bootstrapping options
    if ~isfield(opts,'btsp')
        opts.btsp = 0;
        opts.btsp_variables = {};
        opts.btsp_type = {};
    else
        assert(opts.btsp > 0 && mod(opts.btsp,1) == 0,...
            'opts.btsp should be an integer >= 0')
    end
    if opts.btsp > 0
        assert(isfield(opts,'btsp_variables'),...
            'opts.btsp option requires opts.btsp_variables');
        assert(isfield(opts,'btsp_type'),...
            'opts.btsp option requires opts.btsp_type');
        assert(isfield(opts,'btsp_bias'),...
            'opts.btsp option requires opts.btsp_bias');
        assert(iscell(opts.btsp_variables),...
            'opts.btsp_variables option should be a cell array');
        assert(iscell(opts.btsp_type),...
            'opts.btsp_type option should be a cell array');
        assert(iscell(opts.btsp_bias),...
            'opts.btsp_bias option should be a cell array');
        assert(length(opts.btsp_variables) == length(opts.btsp_type),...
            'cell arrays opts.btsp_variables and opts.btsp_type should have the same size')
        assert(length(opts.btsp_variables) == length(opts.btsp_bias),...
            'cell arrays opts.btsp_variables and opts.btsp_bias should have the same size')
        assert(length(opts.btsp_bias) == length(opts.btsp_type),...
            'cell arrays opts.btsp_bias and opts.btsp_type should have the same size')
        for i = 1:length(opts.btsp_variables)
            assert(strcmp(opts.btsp_variables{i},'S') ||...
                strcmp(opts.btsp_variables{i},'hX') ||...
                strcmp(opts.btsp_variables{i}, 'hY') ||...
                strcmp(opts.btsp_variables{i}, 'hZ') ||...
                strcmp(opts.btsp_variables{i}, 'Y'),...
                ['Elements in opts.btsp_variables{:} should be one of ''S'', ''hX'', ''hY'', ''hZ'' or ''Y''. ',...
                'Specified value ''%s'''], opts.btsp_variables{i});
            assert(strcmp(opts.btsp_type{i},'all') ||...
                strcmp(opts.btsp_type{i},'Sconditioned') ||...
                strcmp(opts.btsp_type{i},'hXconditioned') ||...
                strcmp(opts.btsp_type{i},'hYconditioned') ||...
                strcmp(opts.btsp_type{i},'hZconditioned') ||...
                strcmp(opts.btsp_type{i},'Yconditioned'),...
                ['Elements in opts.btsp_type{:} should be one of ''all'', ''Sconditioned'', ''hXconditioned'', ''hYconditioned'' ''hZconditioned'' or ''Yconditioned''. ',...
                'Specified value ''%s'''], opts.btsp_type{i});
            assert(strcmp(opts.btsp_bias{i},'naive') ||...
                strcmp(opts.btsp_bias{i},'qe') ||...
                strcmp(opts.btsp_bias{i}, 'le'),...
                ['Elements in opts.btsp_variables{:} should be one of ''naive'', ''le'', or ''qe''. ',...
                'Specified value ''%s'''], opts.btsp_bias{i});
            splitstring = split(opts.btsp_type{i},'conditioned');
            conditioning_vars{i} = splitstring{1};
            assert(~strcmp(opts.btsp_variables{i},conditioning_vars{i}),...
                ['Shuffle on variable ''%s'' cannot be performed by conditioning on values of the same variable. ', ...
                'The operation has no meaning.'], opts.btsp_variables{i});
        end
    end      
end
%
% set up split factors and other method-depending vars and
% check that number of draws per number of splits is compatible
% with the size of the data (though unlikely to be otherwise)
if strcmp(opts.bias, 'le')
    split_factors = [1 0.5];
    calculate_bias = true;
elseif strcmp(opts.bias, 'qe')
    split_factors = [1 0.5 0.25];
    calculate_bias = true;
else
    split_factors = [1];
    calculate_bias = false;
end

n_trials = length(S(1,:));
n_splits = numel(split_factors);

for i=2:numel(split_factors)
    sf = split_factors(i);
    n_split_trials = floor(n_trials*sf);
    max_n_draws = round(opts.max_draws_per_split_number/sf);
	warning('off','MATLAB:nchoosek:LargeCoefficient')
    assert(max_n_draws <= nchoosek(n_trials,n_split_trials), ['Number of ',...
        'draws per split number ''opts.max_draws_per_split_number'' specified ',...
        'is higher than allowed given sample size. Max allowed for '...
        'method %s and split factor %g is: %i'],...
        opts.bin_methodX, sf, floor(nchoosek(n_trials,n_split_trials)*sf));
end

if min(S) <= 0
    S = S - min(S) + 1; % avoid negative value for the probabilityDist function
end
% bin neural responses if required
if ~strcmp(opts.bin_methodS, 'none')
    S_b = binr(S, opts.n_binsS, opts.bin_methodS) + 1;
else
    S_b = S + 1;
end

if ~strcmp(opts.bin_methodX, 'none')
    hX_b = binr(hX, opts.n_binsX, opts.bin_methodX) + 1; % The '+ 1' is added because zeroes in the array would cause an error in the probabilityDist function
else
    hX_b = hX + 1;
end

if ~strcmp(opts.bin_methodY, 'none')
    hY_b = binr(hY, opts.n_binsY, opts.bin_methodY) + 1;
    Y_b = binr(Y, opts.n_binsY, opts.bin_methodY) + 1;
else
    hY_b = hY + 1;
    Y_b = Y + 1;
end

if ~strcmp(opts.bin_methodZ, 'none')
    hZ_b = binr(hZ, opts.n_binsZ, opts.bin_methodZ) + 1; % The '+ 1' is added because zeroes in the array would cause an error in the probabilityDist function
else
    hZ_b = hZ + 1;
end

% if multi-dimensional response map it onto a 1D space
if length(hX_b(:,1)) > 1
    hX_b = map_Nd_array_to_1d(hX_b);
end
if length(hY_b(:,1)) > 1
    hY_b = map_Nd_array_to_1d(hY_b);
    Y_b = map_Nd_array_to_1d(Y_b);
end
if length(hZ_b(:,1)) > 1
    hZ_b = map_Nd_array_to_1d(hZ_b);
end
if length(S_b(:,1)) > 1
    S_b = map_Nd_array_to_1d(S_b);
end

% calculate cFIT on unshuffled data
if calculate_bias
    [cFIT.biased.value,...
        cFIT.unbiased.value] = calculate_cFIT(S_b,hX_b,hY_b,Y_b,hZ_b,...
    n_splits,split_factors,n_trials,opts);
else
    [cFIT.biased.value] = calculate_cFIT(S_b,hX_b,hY_b,Y_b,hZ_b,...
    n_splits,split_factors,n_trials,opts);
end

% Calculate cFIT on shuffled data if required
for s = 1:length(opts.btsp_variables)
    % initialize arrays for btsp results
    cFIT.biased.btsp{s}                  = NaN(1,opts.btsp);
    cFIT.unbiased.btsp{s}                = NaN(1,opts.btsp);
        
    switch opts.btsp_variables{s}
        case 'S'
            shuffled_var = S_b;
        case 'hX'
            shuffled_var = hX_b;
        case 'hY'
            shuffled_var = hY_b;
        case 'hZ'
            shuffled_var = hZ_b;
        case 'Y'
            shuffled_var = Y_b;
    end
    switch conditioning_vars{s}
        case 'all'
            % un-conditioned case
            cond_var = [];
        case 'S'
            % S conditioned
            cond_var = S_b;
        case 'hX'
            % hX conditioned
            cond_var = hX_b;
        case 'hY'
            % hY conditioned
            cond_var = hY_b;
        case 'hZ'
            % hZ conditioned
            cond_var = hY_b;
        case 'Y'
            % Y conditioned
            cond_var = Y_b;
    end
    
    for b = 1:opts.btsp
        if isempty(cond_var)
            shuffled_var = shuffled_var(randperm(length(shuffled_var)));
        else
            unique_vals = unique(cond_var);
            for i = 1:length(unique_vals)
                val_inds = find(cond_var == unique_vals(i));
                shuffled_val_inds = val_inds(randperm(length(val_inds)));
                shuffled_var(val_inds) = shuffled_var(shuffled_val_inds);
            end
        end
        if calculate_bias
            switch opts.btsp_variables{s}
                case 'S'
                    [cFIT.biased.btsp{s}(b),...
                        cFIT.unbiased.btsp{s}(b)] = calculate_cFIT(shuffled_var,...
                        hX_b,hY_b,Y_b,hZ_b,n_splits,split_factors,...
                        n_trials,opts);
                case 'hX'
                    [cFIT.biased.btsp{s}(b),...
                        cFIT.unbiased.btsp{s}(b)] = calculate_cFIT(S_b,...
                        shuffled_var,hY_b,Y_b,hZ_b,n_splits,split_factors,...
                        n_trials,opts);
                case 'hY'
                    [cFIT.biased.btsp{s}(b),...
                        cFIT.unbiased.btsp{s}(b)] = calculate_cFIT(S_b,...
                        hX_b,shuffled_var,Y_b,hZ_b,n_splits,split_factors,...
                        n_trials,opts);
                case 'hZ'
                    [cFIT.biased.btsp{s}(b),...
                        cFIT.unbiased.btsp{s}(b)] = calculate_cFIT(S_b,...
                        hX_b,hY_b,Y_b,shuffled_var,n_splits,split_factors,...
                        n_trials,opts);
                case 'Y'
                    [cFIT.biased.btsp{s}(b),...
                        cFIT.unbiased.btsp{s}(b)] = calculate_cFIT(S_b,...
                        hX_b,hY_b,shuffled_var,hZ_b,n_splits,split_factors,...
                        n_trials,opts);
            end
        else
            switch opts.btsp_variables{s}
                case 'S'
                    [cFIT.biased.btsp{s}(b)] = calculate_cFIT(shuffled_var,...
                        hX_b,hY_b,Y_b,hZ_b,n_splits,split_factors,...
                        n_trials,opts);
                case 'hX'
                    [cFIT.biased.btsp{s}(b)] = calculate_cFIT(S_b,...
                        shuffled_var,hY_b,Y_b,hZ_b,n_splits,split_factors,...
                        n_trials,opts);
                case 'hY'
                    [cFIT.biased.btsp{s}(b)] = calculate_cFIT(S_b,...
                        hX_b,shuffled_var,Y_b,hZ_b,n_splits,split_factors,...
                        n_trials,opts);
                case 'hZ'
                    [cFIT.biased.btsp{s}(b)] = calculate_cFIT(S_b,...
                        hX_b,hY_b,Y_b,shuffled_var,n_splits,split_factors,...
                        n_trials,opts);
                case 'Y'
                    [cFIT.biased.btsp{s}(b)] = calculate_cFIT(S_b,...
                        hX_b,hY_b,shuffled_var,hZ_b,n_splits,split_factors,...
                        n_trials,opts);
            end
        end
    end
end
end

%% Helper functions
function [cFIT_biased, varargout] = calculate_cFIT(S_b,hX_b,hY_b,Y_b,hZ_b,...
    n_splits,split_factors,n_trials,opts)

cFIT_splits = zeros(1,n_splits);

if n_splits == 1
    parforArg = 0;
else
    parforArg = Inf;
end

for s = 1:n_splits
    sf = split_factors(s);
    n_split_trials(s) = floor(n_trials*sf);
    if sf == 1
        max_n_draws = 1;
    else
        max_n_draws = round(opts.max_draws_per_split_number/sf);
    end
    cFIT_tmp = zeros(1,max_n_draws);
    
    parfor (d = 1:max_n_draws, parforArg)
        % fetch parallel results as they arrive
        cFIT_tmp(d) = calculate_conditional_feature_specific_information_transmission(S_b,hX_b,hY_b,Y_b,hZ_b,n_trials,n_split_trials(s));
    end
    cFIT_splits(s) = mean(cFIT_tmp);
end
% biased cFIT
cFIT_biased = cFIT_splits(1);
% bias-corrected cFIT
if strcmp(opts.bias, 'le')
    p = polyfit(1./n_split_trials, cFIT_splits, 1);
    varargout{1} = p(2);
elseif strcmp(opts.bias, 'qe')
    p = polyfit(1./n_split_trials, cFIT_splits, 2);
    varargout{1} = p(3);
end
end

