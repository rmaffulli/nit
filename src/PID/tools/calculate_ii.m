% Given 3 variables S, R and C; this function computes the 
% intersection information (II) from S to C trhough R defined as 
%
% II = min(SI({S}{R}_C), SI({C}{R}_S))
%
% where the '_target' notation indicates the target of the information
% decomposition for the considered PID atom
% the function returns as well the PID decomposition PID({S}{R}_C)
% and PID({C}{R}_S)
% II_out is a vector composed of the following quantities:
% II_out(1): II
% II_out(2): PID_RC_S_sh (shared)
% II_out(3): PID_RC_S_ur (unique R)
% II_out(4): PID_RC_S_uc (unique C)
% II_out(5): PID_RC_S_co (complementary)
% II_out(6): PID_RS_C_sh (shared)
% II_out(7): PID_RS_C_ur (unique R)
% II_out(8): PID_RS_C_us (unique S)
% II_out(9): PID_RS_C_co (complementary)


function [II_out] = calculate_ii(S,C,R,n_trials,n_split_trials)
% random sample n_split_trials from data in each of the
% n_draws and calculate II on the reduced dataset
ri = randperm(n_trials, n_split_trials);
Sr = S(ri);
Cr = C(ri);
Rr = R(ri);
[prob_matrix, ~, nS, nR, nC, n_s_d] = build_p(Sr, Rr, Cr);
python_ComputeII = py.ComputePID.ComputeII(prob_matrix,...
    nS, nR, nC,...
    n_s_d);
II_out = cell2mat(cell(python_ComputeII.calculate()));
end