function [measure_null,pval] = shuffling_null_hypothesis(data, value, opts)
%%
% This function compute the 'conditional independence given the stimulus'
% null hypothesis for the intersection information (II) and the 
% feature-specific information transmission (FIT) measures.

% Inputs:
% data should be a N x nTrials array where:
% For II --> N = 3. row 1 = stimulus; row 2 = neural respose; row 3 = choices
% For FIT --> N = 4. row 1 = stimulus; row 2 = emitter past; row 3 = receiver past; row 4 = receiver present
% For cFIT --> N = 5. row 1 = stimulus; row 2 = emitter past; row 3 = receiver past; row 4 = receiver present; row 5 = confounder past
% value: information value on the original distribution

if size(data,1) == 3
    infotype = 'II';
    S = data(1,:)';
    X = data(2,:);
    C = data(3,:)';
elseif size(data,1) == 4
    infotype = 'FIT';
    S = data(1,:)';
    X = data(2,:);
    hY = data(3,:);
    Y = data(4,:);
elseif size(data,1) == 5
    infotype = 'cFIT';
    S = data(1,:)';
    X = data(2,:);
    hY = data(3,:);
    Y = data(4,:);
    hZ = data(5,:);
end

n_permutations = opts.nh_perm;

nTrials = size(S,1);

measure_null = zeros(1, n_permutations);

for sidx = 1:n_permutations
    
    XSh = randperm(X);
    
    if strcmp(infotype, 'II')
        % n_split_trials = nTrials because we compute the null hypothesis for the finite-sampling biased measure
        measure_null(sidx) = calculate_intersection_information(S, C, XSh, nTrials, nTrials);
    elseif strcmp(infotype, 'FIT')
        measure_null(sidx) = calculate_feature_specific_information_transmission(S, XSh, hY, Y, nTrials, nTrials);
    elseif strcmp(infotype, 'cFIT')
        measure_null(sidx) = calculate_conditional_feature_specific_information_transmission(S, XSh, hY, Y, hZ, nTrials, nTrials);
    end
end
pval = mean(measure_null >= value);

end