% Given 4 variables S, hX, hY and Y; this function computes the 
% information transmission measure defined in Beer and Williams (2015) 
%
% where the '_target' notation indicates the target of the information
% decomposition for the considered PID atom

function [info_transm] = calculate_information_transmission_WB2015(S,hX,hY,Y,n_trials,n_split_trials)
        % random sample n_split_trials from data in each of the
        % n_draws and calculate II on the reduced dataset
        ri = randperm(n_trials, n_split_trials);
        Sr = S(ri);
        hXr = hX(ri);
        hYr = hY(ri);
        Yr = Y(ri);
        [p_hXYhYS] = probabilityDist(hXr, Yr, hYr, Sr');
        info_transm = compute_WB2015_atoms(p_hXYhYS);
end