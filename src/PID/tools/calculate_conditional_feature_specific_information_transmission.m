% Given 4 variables S, hX, hY and Y; this function computes the 
% feature-specific information transmission (FIT) defined as 
%
% FIT = min({hX}{Y}_S, {hX}{S}_Y) - min({hX}{Y}{hZ}_S, {hX}{S}{hZ}_Y)
%
% where the '_target' notation indicates the target of the information
% decomposition for the considered PID atom

function [cFIT] = calculate_conditional_feature_specific_information_transmission(S,hX,hY,Y,hZ,n_trials,n_split_trials)
%%        
        % random sample n_split_trials from data in each of the
        % n_draws and calculate II on the reduced dataset
        ri = randperm(n_trials, n_split_trials);
        Sr = S(ri);
        hXr = hX(ri);
        hYr = hY(ri);
        hZr = hZ(ri);
        Yr = Y(ri);
        % FIT through X
        [p_hXYhYS] = probabilityDist(hXr, Yr, hYr, Sr);
        FIT_S = computePiXY(p_hXYhYS);
        p_hXShYY = permute(p_hXYhYS, [1 4 3 2]);
        FIT_Y = computePiXY(p_hXShYY);
        FIT = min([FIT_S, FIT_Y]);
        
        % FIT through Z
        [p_hZYhYS] = probabilityDist(hXr, Yr, hYr, Sr);
        FITZ_S = computePiXY(p_hZYhYS);
        p_hZShYY = permute(p_hZYhYS, [1 4 3 2]);
        FITZ_Y = computePiXY(p_hZShYY);
        FITZ = min([FIT_S, FIT_Y]);
        
        pXYhYhZS = probabilityDist3var(hXr, Yr, hYr, hZr, Sr);
        pXShYhZY = permute(pXYhYhZS, [1 5 3 4 2]);
        
        FIT_sharedZS = calculate_redundant_confounder_information(pXYhYhZS);
        FIT_sharedZY = calculate_redundant_confounder_information(pXShYhZY);
        cFIT = FIT - min([FIT_sharedZS, FIT_sharedZY]);
end