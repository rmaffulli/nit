% Given 3 variables Y, X1 and X2; this function computes the 
% partial information decomposition of X1 and X2 about Y


function PID = calculate_pid(Y,X1,X2,n_trials,n_split_trials)
        % random sample n_split_trials from data in each of the
        % n_draws and calculate II on the reduced dataset
        ri = randperm(n_trials, n_split_trials);
        Yr = Y(ri);
        X1r = X1(ri);
        X2r = X2(ri);
        [prob_matrix, ~, nY, nX1, nX2, n_s_d] = build_p(Yr', X1r', X2r');
        python_ComputePID = py.ComputePID.ComputePID(prob_matrix,...
             nY, nX1, nX2,...
             n_s_d);
        PID = cell2mat(cell(python_ComputePID.calculate()));
end