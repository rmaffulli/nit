function sharedFIThZ = calculate_redundant_confounder_information( pxyhyhzs )
% Here we compute the {hX}{Yt}{hZ} PID atom. This has to be subtracted from
% FIT_min in order to obtain the correction method 1c

% In the coming code abbreviations in the names of variables are as
% follows:

% r -> denotes one set of synergy. Basically stands for {} but they
% cannot be in a name of a variable. rX -> {X}, rXrYhY -> {X}{Y, hY}
% X -> past of the sender
% Y -> current of the receiver
% hY -> past of the receiver
% hZ -> past of the confounder
% ps -> probability of S - P(S)
% psa -> P(S|A)
% pas -> P(A|S)
% PiR -> greek letter Pi with index R as used in the paper


% Check whether the matrix is a valid probability distribution matrix (sum = 1)
% assert(abs(sum(pxyhys(:)) - 1) > eps, 'The matrix is not a valid joint probability distribution');

% Compute specific surprises fro all subsets of {X, Y, hY}

specSurpriseX = zeros(size(pxyhyhzs, 5), 1);
specSurpriseY = zeros(size(pxyhyhzs, 5), 1);
specSurprisehY = zeros(size(pxyhyhzs, 5), 1);
specSurprisehZ = zeros(size(pxyhyhzs, 5), 1);


% For all possible stimuli compute all these matrices
for s = 1:size(pxyhyhzs, 5)
    
    ps(s) = sum(sum(sum(sum(pxyhyhzs(:, :, :, :, s))))); % probability of S
            
    % {x}
    for x = 1:size(pxyhyhzs, 1)
        psa = sum(sum(sum(sum(pxyhyhzs(x, :, :, :, s))))) / (sum(sum(sum(sum(pxyhyhzs(x, :, :, :, :))))) + eps);
        pas = sum(sum(sum(sum(pxyhyhzs(x, :, :, :, s))))) / (sum(sum(sum(sum(pxyhyhzs(:, :, :, :, s))))) + eps);
        specSurpriseX(s) = specSurpriseX(s) + pas * (log2(1/(ps(s) + eps)) - log2(1/(psa + eps)));
    end
            
    % {y}
    for y = 1:size(pxyhyhzs, 2)
        psa = sum(sum(sum(sum(sum(pxyhyhzs(:, y, :, :, s)))))) / (sum(sum(sum(sum(sum(pxyhyhzs(:, y, :, :, :)))))) + eps);
        pas = sum(sum(sum(sum(sum(pxyhyhzs(:, y, :, :, s)))))) / (sum(sum(sum(sum(sum(pxyhyhzs(:, :, :, :, s)))))) + eps);
        specSurpriseY(s) = specSurpriseY(s) + pas * (log2(1/(ps(s) + eps)) - log2(1/(psa + eps)));
    end
    
    % {hy}
    for hy = 1:size(pxyhyhzs, 3)
        psa = sum(sum(sum(sum(sum(pxyhyhzs(:, :, hy, :, s)))))) / (sum(sum(sum(sum(sum(pxyhyhzs(:, :, hy, :, :)))))) + eps);
        pas = sum(sum(sum(sum(sum(pxyhyhzs(:, :, hy, :, s)))))) / (sum(sum(sum(sum(sum(pxyhyhzs(:, :, :, :, s)))))) + eps);
        specSurprisehY(s) = specSurprisehY(s) + pas * (log2(1/(ps(s) + eps)) - log2(1/(psa + eps)));
    end
    
    % {hz}
    for hz = 1:size(pxyhyhzs, 4)
        psa = sum(sum(sum(sum(sum(pxyhyhzs(:, :, :, hz, s)))))) / (sum(sum(sum(sum(sum(pxyhyhzs(:, :, :, hz, :)))))) + eps);
        pas = sum(sum(sum(sum(sum(pxyhyhzs(:, :, :, hz, s)))))) / (sum(sum(sum(sum(sum(pxyhyhzs(:, :, :, :, s)))))) + eps);
        specSurprisehZ(s) = specSurprisehZ(s) + pas * (log2(1/(ps(s) + eps)) - log2(1/(psa + eps)));
    end
    

end

% Computation of iMin as defined in the WB paper for all nodes in the lattice
iMinrXrYrhYrhZ = 0;
iMinrXrYrhZ = 0;


for s = 1:size(pxyhyhzs, 5)
    % {X}{Y}{hYhZ}
    iMinrXrYrhYrhZ = iMinrXrYrhYrhZ + ps(s) * min([specSurpriseX(s) specSurpriseY(s) specSurprisehY(s) specSurprisehZ(s)]);
    
    % {X}{Y}
    iMinrXrYrhZ = iMinrXrYrhZ + ps(s) * min([specSurpriseX(s) specSurpriseY(s) specSurprisehZ(s)]);
end

% PiR term for {X}{Y}{hY} is just its iMin
% The next PiR terms are computed as iMin - sum of all preceding PiR terms
% in the lattice. It has to be computed from below up, of course.

sharedFIThZ = iMinrXrYrhZ - iMinrXrYrhYrhZ;