function [MI,varargout] = NPCInformation(X, Y, opts)
%%% *function [MI,copula] = copulaInformation(X, Y, opts)*
%%%
%%% ### Description
%%% Compute mutual information using mixed-vine or non-parametric copulae.
%%%
%%% ### Inputs:
%%% - *X*: *nDimensions X nTrials* array including input data (typically the neural response).
%%% - *Y*: *1 X nTrials* array including input data (typically the stimulus). 
%%% - *opts*: options sctructure:
%%%
%%%     |      Field     | Optional |                                 Description                                                                   | Default Value |
%%%     |:--------------:|:--------:|:-------------------------------------------------------------------------------------------------------------:|:-------------:|
%%%     |  opts.margins  |   False  | *'cont'* and *'discrete'* for continuous or discrete marginals respectively                                   |       —       |
%%%     |     opts.bw    |   True   | *'LL1'* or *'LL2'* bandwidth methods                                                                          |     'LL1'     |
%%%     | opts.knots_fit |   True   | number of bins used in fitting the copula                                                                     |      100      |
%%%     | opts.knots_est |   True   | number of bins used in estimating the copula                                                                  |      100      |
%%%     |   opts.n_reps  |   True   | number of data replications to be used in the information estimation                                          |       1       |
%%%     |  opts.parallel |   True   | *0* for non parallel and *1* parallel computing                                                               |       0       |
%%%     |   opts.alpha   |   True   | alpha for monte-carlo sampling variance                                                                       |      0.05     |
%%%     |   opts.erreps  |   True   | variance threshold for monte-carlo sampling for information estimation                                        |      1e-3     |
%%%     |    opts.iter   |   True   | maximum number of monte-carlo iterations                                                                      |       50      |
%%%     |   opts.cases   |   True   | number of samples in each iteration                                                                           |     20000     |
%%%     |  opts.verbose  |   True   | display extended output info                                                                                  |     False     |
%%%     |   opts.btsp    |   True   | number of bootstrapping estimation steps for null-distribution estimation                                     |       0       |
%%%     |   opts.bias    |   True   | *'naive'* without correction, *'btsp'* removes the mean of bootstrapped MI, *'qe'* for quadratic extrapolation|    'naive'    |
%%%     | opts.max_draws_per_split_number|   True   | specifies the maximum number of draws on which to calculate the unbiased estimate with 'qe'.  |      50       |

%%%
%%% ### Outputs:
%%% - *MI*: mutual information estimated from the copula
%%% - *copula*: copula fitted to X and Y. The data structure will be different depending on the used copula structure (NPC Vs MVC).
%
%  This source code is part of:
%  NIT - Neuroscience Information Toolbox
%  Copyright (C) 2020  Roberto Maffulli, Miguel Angel Casal Santiago
%
%  This program is free software: you can redistribute it and/or modify
%  it under the terms of the GNU General Public License as published by
%  the Free Software Foundation, either version 3 of the License, or
%  (at your option) any later version.
%
%  This program is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%  GNU General Public License for more details.
% 
%  You should have received a copy of the GNU General Public License
%  along with this program.  If not, see <https://www.gnu.org/licenses/>.

%input checking
assert(nargin>=3,'Not enough input arguments')
assert(nargout<3, 'Too many output arguments')

assert(size(Y,2)==size(X,2),'Amount of trials is not consistent between X and Y')

if any(isnan(X))
    error("X contains NaNs. Aborting.")
end
if any(isnan(Y))
    error("Y contains NaNs. Aborting.")
end
if ~isfield(opts, 'margins')
        error('Missing mandatory argument: please specify type of marginal distributions.')
else
    for i = 1:length(opts.margins)
       if ~contains(opts.margins{i}, ["cont" "discrete"])
           error("Specified marginals do not match any of the possible options. Use 'cont' for continuous marginals and 'discrete' for discrete ones.")
       end
    end
end
if ~isfield(opts, 'verbose')
    opts.verbose = false;
end
    
warn = 0;
if size(X,1)>1
    error('Sorry, but non-parametric copula can only be used with one-dimensional data')
end
if ~isfield(opts, 'margins')
    error('Please provide information about the type of marginals in opts.margins')
end
if ~isfield(opts, 'bw')
    warn = 1;
    opts.bw = 'LL1';
end
if ~isfield(opts, 'knots_fit')
    warn = 1;
    opts.knots_fit = 30;
end
if ~isfield(opts, 'n_reps')
    warn = 1;
    opts.n_reps = 1;
end
if ~isfield(opts, 'knots_est')
    opts.knots_est = opts.knots_fit;
end
if ~isfield(opts, 'parallel')
    warn = 1;
    opts.parallel=0;
end
if ~isfield(opts, 'alpha')
    warn = 1;
    opts.alpha=0.05;
end
if ~isfield(opts, 'erreps')
    warn = 1;
    opts.erreps=1e-3;
end
if ~isfield(opts, 'iter')
    warn = 1;
    opts.iter=2;
end
if ~isfield(opts, 'cases')
    warn = 1;
    opts.cases=5000;
end
if ~isfield(opts, 'btsp')
    warn = 1;
    opts.btsp = 0;
end
if ~isfield(opts, 'bias')
    warn = 1;
    opts.bias = 'naive';
end
opts.plot = 0;
if warn
    warning('Not all the parameters were provided in the options structure. Using default values instead.')
end
assert(strcmp(opts.bias,'naive') ||...
    strcmp(opts.bias, 'btsp') ||...
    strcmp(opts.bias, 'qe'),...
    ['opts.bias argument can be only ''naive'', ''btsp'' or ''qe''. ',...
    'Specified value ''%s'''], opts.bias);

if strcmp(opts.bias, 'btsp')
    assert(opts.btsp > 0, ...
        ['opts.bias = ''btsp'' argument requires opts.btsp > 0 . ',...
        'Specified value ''%d'''], opts.btsp);
end

if strcmp(opts.bias, 'qe')
    if ~isfield(opts, 'max_draws_per_split_number')
        opts.max_draws_per_split_number = 50
    end
end 

% merge X and Y in a single X to apply transformation in case of a discrete marginal (marginals are given in the same order as inputs)
X = [X',Y'];

% convert discrete values to continous if necessary
for i = 1:length(opts.margins)
    if strcmp(string(opts.margins{i}), "discrete")
        X(:,i) = NPC_discrete_to_cont(X(:,i));
        opt.margins{i} = 'cont';
    end
end

% swap response and stimuli to match NPC inputs requirement
X = [X(:,2) X(:,1)];

if opts.verbose
    disp('Building the vine structure...')
end

% Build the vine structure
[vine]=NPC_prep_copula(X,opts.margins,opts.n_reps);

if opts.verbose
    disp('Fitting the copula bandwidths (this may take some time)...')
end
% Fit bandwidths of copula to data:
[~,~,copula_unshuffled,~,~] = NPC_Fit_vCopula(vine,X(1,:),opts.bw,1,0,opts.knots_fit,opts.parallel);

if opts.verbose
    disp('Estimating copula over grid and data points (this may take some time)...')
end
% Estimate copula:
[~,~,copula,~,~] = NPC_Fit_vCopula(vine,X(1,:),opts.bw,-1,copula_unshuffled,opts.knots_est,opts.parallel);

if opts.verbose
    disp('Estimating Mutual Information (this may take some time)...')
end
% Calculate MI:
[MI,~,~,~] = NPC_kernelvineinfo(vine,copula,opts);

if strcmp(opts.bias,'qe')
    % calculate qe corrected MI
    split_factors = [1 0.5 0.25];
    parforArg = Inf;
    n_trials = length(Y);
    for i=2:numel(split_factors)
        sf = split_factors(i);
        n_split_trials = floor(n_trials*sf);
        max_n_draws = round(opts.max_draws_per_split_number/sf);
        warning('off','MATLAB:nchoosek:LargeCoefficient')
        assert(max_n_draws <= nchoosek(n_trials,n_split_trials), ['Number of ',...
            'draws per split number ''opts.max_draws_per_split_number'' specified ',...
            'is higher than allowed given sample size. Max allowed for '...
            'split factor %g is: %i'],...
            sf, floor(nchoosek(n_trials,n_split_trials)*sf));
    end
    MI_splits = zeros(1,length(split_factors));
    MI_splits(1) = MI;
    n_split_trials = zeros(1,length(split_factors));
    n_split_trials(1) = n_trials;
    for s=2:numel(split_factors)
        sf = split_factors(s);
        n_split_trials(s) = floor(n_trials*sf);
        max_n_draws = round(opts.max_draws_per_split_number/sf);
        
        MI_temp = zeros(1,max_n_draws);
        
        % parfor (d = 1:max_n_draws, parforArg)
        for d = 1:max_n_draws
            % fetch parallel results as they arrive
            ri = randperm(n_trials, n_split_trials(s));
            Xr = X(ri,:);

            [vine]=NPC_prep_copula(Xr,opts.margins,opts.n_reps);
            [~,~,copula_unshuffled,~,~] = NPC_Fit_vCopula(vine,Xr(1,:),opts.bw,1,0,opts.knots_fit,opts.parallel);
            [~,~,copula,~,~] = NPC_Fit_vCopula(vine,Xr(1,:),opts.bw,-1,copula_unshuffled,opts.knots_est,opts.parallel);
            [MI_temp(d),~,~,~] = NPC_kernelvineinfo(vine,copula,opts);
        end
        MI_splits(s) = mean(MI_temp);
    end
    p = polyfit(1./n_split_trials, MI_splits, 2);
    MI = p(3);
end

for i = 1:opts.btsp
    X_shuffle = [X(randperm(length(X(:,1))),1),...
        X(randperm(length(X(:,1))),2)]; % shuffle the data
    
    % fit vine structure on shuffled data
    [vine_shuffled]=NPC_prep_copula(X_shuffle,opts.margins,n_reps);
    
    % Estimate copula on shuffled data (here we use vine from shuffled data but bandwidth from unshuffled data)
    [~,~,copula_shuffled,~,~] = NPC_Fit_vCopula(vine_shuffled,X_shuffle(1,:),opts.bw,-1,copula_unshuffled,opts.knots_est,opts.parallel);
    
    % Calculate MI on shuffled data
    [MI(i+1),~,~,~] = NPC_kernelvineinfo(vine_shuffled,copula_shuffled,opts);
end

% estimate bias as the mean of bootstrapped estimate if required
if strcmp(opts.bias,'btsp')
    MI(1) = MI(1) - mean(MI(2:end));
end

if nargout > 1
    varargout{1} = copula;
end

end

