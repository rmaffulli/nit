function [optbins, MI] = optimize_n_bins(X, Y, opts)
%%% *function [optbins, MI] = optimize_n_bins(X, Y, opts)*
%%%
%%% ### Description
%%% Given a range for the number of bins to be used, this function returns
%%% the optimal number of bins for calculation of MI using binned methods. 
%%% Here 'optimal' is indended as the number of bins that maximizes the
%%% bias corrected MI content between *X* and *Y*. If *Y* is a continuous
%%% signal the optimization is ran on a 2D sweep.
%%%
%%% ### Inputs:
%%% - *X*: *nDimensionsX X nTrials* array including input data (typically the neural response).
%%% - *Y*: *nDimensionsY X nTrials* array including input data (typically the stimulus). In principle the values of Y can also represent a multi-dimensional neural response (if the MI between two neuronal populations is of interest). While the MI calculated is symmetrical to swapping X and Y, the user should be aware that the information breakdown quantities are calculated only on X. 
%%% - *opts*: options structure (see further notes section for more details).
%%%
%%% ### Outputs:
%%% - *optbins*: output structure returning the calculated optimal number of bins for X and Y (the latter is returned only if required).
%%% - *MI*: values of bias corrected MI for each of the number of bins in the parmetric sweep. 1D array if *Y* is a discrete signal, 2D matrix if *Y* is continuous.
%%%
%%% ### Further notes
%%% #### The options structure
%%% The options structure can include any the following fields:
%%%
%%% | field              | description                                                                                                                                                                                                                                                                                                                     | allowed values                                                                            | default                                       |
%%% |--------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------|-----------------------------------------------|
%%% | opts.bin_range_X   | specifies the range of bins for investigation of X variable                                                                                                                                                                                                                                                                     | Vector of integer values                                                                  | `[2:10]`                                      |
%%% | opts.bin_range_Y   | specifies the range of bins for investigation of Y variable                                                                                                                                                                                                                                                                     | Vector of integer values or empty vector (Y is discrete)                                  | `[2:10]`                                      |
%%% | opts.bin_methodX   | binning method for X (can be a scalar or a cell array of `size(method) = size(X,1)`). If a scalar is specified then all dimensions are binned using the same method. Alternatively, each dimension is binned using its specific method. If not specified it is assumed that no binning is necessary and X is already discrete.  | see the documentation of [`information`](src/MI/BinnedMethods) function for more details  | `'eqspace'`                                   |  
%%% | opts.bin_methodY   | binning method for Y (can be a scalar or a cell array of `size(method) = size(Y,1)`). If a scalar is specified then all dimensions are binned using the same method. Alternatively, each dimension is binned using its specific method. If not specified it is assumed that no binning is necessary and Y is already discrete.  | see the documentation of [`information`](src/MI/BinnedMethods) function for more details  | `'none'`(Y is assumed as discrete by default) |
%%% | opts.plotresults   | boolean value to plot results                                                                                                                                                                                                                                                                                                   | bool                                                                                      | `'false'`                                     |
%%%
%
%  This source code is part of:
%  NIT - Neuroscience Information Toolbox
%  Copyright (C) 2020  Roberto Maffulli, Miguel Angel Casal Santiago
%
%  This program is free software: you can redistribute it and/or modify
%  it under the terms of the GNU General Public License as published by
%  the Free Software Foundation, either version 3 of the License, or
%  (at your option) any later version.
%
%  This program is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%  GNU General Public License for more details.
% 
%  You should have received a copy of the GNU General Public License
%  along with this program.  If not, see <https://www.gnu.org/licenses/>.


% check inputs dimensions
assert(length(Y(1,:)) == length(X(1,:)),...
    "Number of trials are differing in X and Y");

% check for nans
if any(isnan(Y))
    error("Y contains NaNs. Aborting.")
end
if any(isnan(X))
    error("X contains NaNs. Aborting.")
end

% set default options
if ~isfield(opts,'bin_methodX')
    info_opts.bin_methodX = 'eqspace';
else
    info_opts.bin_methodX = opts.bin_methodX;
end

if ~isfield(opts,'bin_range_X')
    opts.bin_range_X = [2:10];
    info_opts.bin_methodY = 'eqspace';
end

if ~isfield(opts,'bin_methodY') && ~isfield(opts,'bin_range_Y')
    info_opts.bin_methodY = 'none';
    opts.bin_range_Y = 1;
elseif ~isfield(opts,'bin_range_Y')
    opts.bin_range_Y = [2:10];
elseif ~isfield(opts,'bin_methodY')
    info_opts.bin_methodY = 'eqspace';
end

if ~isfield(opts,'plotresults')
    opts.plotresults = false;
end

info_opts.bias = 'pt';
info_opts.method = 'dr';
info_opts.verbose = false;
MI = NaN(length(opts.bin_range_X), length(opts.bin_range_Y));


for x = 1:length(opts.bin_range_X)
    info_opts.n_binsX = opts.bin_range_X(x);
    for y = 1:length(opts.bin_range_Y)
        info_opts.n_binsY = opts.bin_range_Y(y);
        info_opts.btsp = 50;
        mi_out = information(X,Y,info_opts,{'I'});
        mi_out = mi_out{1};
        p_val = sum(mi_out(2:end) >= mi_out(1))/info_opts.btsp;
        if p_val < 0.05
            MI(x,y) = mi_out(1);
        else
            MI(x,y) = NaN;
        end
    end
end

if length(opts.bin_range_Y) > 1
    if all(isnan(MI))
        optbins.n_binsX = NaN;
        optbins.n_binsY = NaN;
    else
        [~,max_y] = max(max(MI));
        [~,max_x] = max(MI(:,max_y));
        optbins.n_binsX = opts.bin_range_X(max_x);
        optbins.n_binsY = opts.bin_range_Y(max_y);
    end
    
    
    if opts.plotresults
        figure()
        imagesc(MI')
        h = colorbar;
        ylabel(h,"MI(X;Y)")
        xlabel("N. bins X")
        ylabel("N. bins Y")
    end
else
    if all(isnan(MI))
        optbins.n_binsX = NaN;
    else
        [~, max_x] = max(MI);
        optbins.n_binsX = opts.bin_range_X(max_x);
    end
    
    if opts.plotresults
        figure()
        plot(opts.bin_range_X,MI)
        xlabel("N. bins X")
        ylabel("MI(X;Y)")
    end
end

end



