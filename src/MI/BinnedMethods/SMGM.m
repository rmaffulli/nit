function info = SMGM(X,Y)
%%% *function info = SMGM(X,Y)*
%%%
%%% ### Description
%%% Compute Skaggs information metric in bits/sec (see W. Skaggs et al. "An information-theoretic approach to deciphering the hippocampal code." Advances in neural information processing systems 5, 1992).
%%%
%%% ### Inputs:
%%% - *X*: *1 X nTrials* array defining the neural response.
%%% - *Y*: *1 X nTrials* array defining the stimulus (which must be discrete).
%%%
%%% ### Outputs:
%%% - *info*: value of SMGM information metric in bits/sec.
%%%
%
%  This source code is part of:
%  NIT - Neuroscience Information Toolbox
%  Copyright (C) 2020  Roberto Maffulli, Miguel Angel Casal Santiago
%
%  This program is free software: you can redistribute it and/or modify
%  it under the terms of the GNU General Public License as published by
%  the Free Software Foundation, either version 3 of the License, or
%  (at your option) any later version.
%
%  This program is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%  GNU General Public License for more details.
% 
%  You should have received a copy of the GNU General Public License
%  along with this program.  If not, see <https://www.gnu.org/licenses/>.

info = 0;
r_mean = mean(X);
unique_s = unique(Y);
for s = 1:length(unique_s)
    this_r_mean = mean(R(S==unique_s(s)));
    p_s = sum(S==unique_s(s))/length(R);
    out = out + this_r_mean*p_s*log2(this_r_mean/r_mean);
end
end