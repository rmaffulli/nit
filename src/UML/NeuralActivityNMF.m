function out = NeuralActivityNMF(spikes,opts)
%%% *out = NeuralActivityNMF(spikes,opts)*
%%%
%%% ### Description
%%% Applies non-negative Matrix Factorization to a multi-dimensional neural dataset. The dataset must consist of spike trains. 
%%% The function allows to perform both space-time (`'ST'`) and space-by-time (`'SbyT'`) factorization through user input.
%%%
%%% ### Inputs:
%%% - *spikes*: *nStimulus X 1* cell array. Each cell contains a *nCells X nTrials* cell array specifying the spike times in milliseconds.
%%% - *opts*: options structure (see further notes section for more details).
%%%
%%% ### Outputs:
%%% - *out*: structure containing:
%%%         - *activationCoeffsTrain*: activation coefficients for the train subset
%%%         - *activationCoeffsTest*: activation coefficients for the test subset
%%%         - *factorization*: wether `'ST'` or `'SbyT'` factorization
%%%         - if 'ST' factorization:
%%%                 - *nModules*: number of modules used
%%%                 - *modules*: fitted modules
%%%         - if 'SbyT' factorization:
%%%                 - *nModulesSpace*: number of spatialmodules used
%%%                 - *nModulesTime*: number of time modules used
%%%                 - *modulesSpace*: fitted spatial modules
%%%                 - *modulesTime*: fitted time modules
%%%                 - *vaf*: variance explained
%%%                 - *err*: total reconstruction error
%%%
%%% ### Further notes:
%%% The *opts* structure can have the following fields:
%%%
%%% | field                                | description                                                                                        | allowed values                                                | default   |
%%% |--------------------------------------|----------------------------------------------------------------------------------------------------|---------------------------------------------------------------|-----------|
%%% | opts.bin_size                        | length of a bin in milliseconds                                                                    | int > 0                                                       | `none`    |
%%% | opts.len_trial                       | length of a trial in milliseconds                                                                  | int > 0                                                       | `none`    |
%%% | opts.n_trials                        | number of trials                                                                                   | int > 1                                                       | `none`    |
%%% | opts.n_neurons                       | number of neurons                                                                                  | int > 2                                                       | `none`    |
%%% | opts.factorization                   | type of factorization                                                                              | `'ST'` (spatiotemporal)<br> `'SbyT'` (space by time)<br>`     | `'ST'`    |
%%% | opts.optimizeModules                 | search for optimal number of modules or not                                                        | `true` (optimize)<br>  `false` (do not optimize)<br>          | `true`    |
%%% | opts.nModulesST                      | number of modules for ST factorization (only if optimizeModules=false and factorization='ST')      | int>1                                                         | `none`    |
%%% | opts.nModulesSbyT                    | number of modules for SbyT factorization (only if optimizeModules=false and factorization='SbyT')  | [int>1 (number space modules), int>1 (number time modules)]   | `none`    |
%
%  This source code is part of:
%  NIT - Neuroscience Information Toolbox
%  Copyright (C) 2020  Roberto Maffulli, Miguel Angel Casal Santiago
%
%  This program is free software: you can redistribute it and/or modify
%  it under the terms of the GNU General Public License as published by
%  the Free Software Foundation, either version 3 of the License, or
%  (at your option) any later version.
%
%  This program is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%  GNU General Public License for more details.
% 
%  You should have received a copy of the GNU General Public License
%  along with this program.  If not, see <https://www.gnu.org/licenses/>.

%% check flags

assert(size(spikes,2)==1,...
    'spikes variable must be a column cell array')
assert(size(spikes,1)>1,...
    'spikes variable must be a column cell array')

assert( nargin == 2 ,...                                        % check number of inputs
    'The amount of inputs is not correct')
assert( isfield(opts, 'bin_size') == 1 ,...                     % check bin size
    'Please provide the bin size in the options structure')
assert(opts.bin_size>0,...                                      % check bin size
    'Bin size cannot be 0 or negative')
assert( isfield(opts, 'len_trial') == 1 ,...                    % check len trial
    'Please provide the trial length in the options structure')
assert(opts.len_trial>0 ,...                                    % check len trial
    'Trial duration cannot be 0 or negative')
assert( isfield(opts, 'n_trials') == 1 ,...                    % check number of trials
    'Please provide the number of trials in the options structure')
assert(opts.n_trials>1 ,...                                    % check number of trials
    'Number of trials cannot be lower than 2')
assert( isfield(opts, 'n_neurons') == 1 ,...                    % check number of neurons
    'Please provide the number of neurons in the options structure')
assert(opts.n_neurons>2 ,...                                    % check number of neurons
    'Number of neurons must be higher than 2')
if ~isfield(opts, 'factorization') || (strcmp(opts.factorization,'ST')==0 && strcmp(opts.factorization,'SbyT')==0)
    warning('ST factorization will be used. Check whether you provided a correct parameter')
    opts.factorization = 'ST';
end

if ~isfield(opts, 'optimizeModules')
    opts.optimizeModules=true;
    warning('Number of modules will be optimized')
else
    if opts.optimizeModules == false
        if strcmp(opts.factorization,'ST')==1
            if ~(isfield(opts, 'nModulesST') && length(opts.nModulesST)==1 && opts.nModulesST>1)
                error('nModulesST is not provided or is not valid')
            end
            
        elseif strcmp(opts.factorization,'SbyT')==1
            if ~(isfield(opts, 'nModulesSbyT') && length(opts.nModulesSbyT)==2 && opts.nModulesSbyT(1)>1 && opts.nModulesSbyT(2)>1)
                error('nModulesSbyT is not provided or is not valid')
            end
        end
        
    elseif opts.optimizeModules == true
        if isfield(opts, 'nModulesST') || isfield(opts, 'nModulesSbyT')
            warning('Provided modules will not be used because optimization has been set')
        end
    end
end
%%
% Bin size in milliseconds
bin_size = opts.bin_size;
% Length of a trial in milliseconds
len_trial = opts.len_trial;
% number of stimuli
n_stimuli = length(spikes);
% number of trials
n_trials = opts.n_trials;
% number of neurons
n_neurons= opts.n_neurons;

%% Transform spike trains to spike count matrices
% disp('Transforming spike trains to count matrices...');
counts = cell(n_stimuli,1);
for i = 1:n_stimuli
    counts{i} = count_spikes(spikes{i},bin_size,len_trial);
end

clear i;

%% Randomly separate data into training set and test set
p = randperm(n_trials); % Random permutation of trials
% Training indices
ind_train = p(1:ceil(n_trials/2));
% Test indices
ind_test = p((ceil(n_trials/2)+1):end);

% Total number of training samples
n_e_train = n_stimuli*length(ind_train);
% Total number of test samples
n_e_test = n_stimuli*length(ind_test);
% Number of bins per trial
n_bins = ceil(len_trial/bin_size);

clear p;

%% Build overall data matrices
% disp('Concatenating data...');
% Training set
X_train = zeros(n_neurons,n_e_train*n_bins);
offset = 0;
for i = 1:n_stimuli
    for j = 1:length(ind_train)
        X_train(:,offset+(1:n_bins)) = counts{i}{ind_train(j)};
        offset = offset + n_bins;
    end
end
% Training class labels
groups_train = ceil((1:n_e_train)' / length(ind_train));

% Test set
X_test = zeros(n_neurons,n_e_test*n_bins);
offset = 0;
for i = 1:n_stimuli
    for j = 1:length(ind_test)
        X_test(:,offset+(1:n_bins)) = counts{i}{ind_test(j)};
        offset = offset + n_bins;
    end
end
% Test class labels
groups_test = ceil((1:n_e_test)' / length(ind_train));

clear offset i j;

%%
if strcmp(opts.factorization,'ST')
    % Apply spatiotemporal NMF to factorize data
%     disp(['Optimizing number of spatiotemporal modules.'...
%         ' This can take a while...']);
    if opts.optimizeModules == true
        % Find optimal number of spatiotemporal modules depending on stimulus
        % classification
        n_m = select_n_m(X_train,groups_train,n_e_train,X_test,groups_test,...
            n_e_test);
    else
        n_m = opts.nModulesST;
    end
    % Obtain spatiotemporal modules from training set
    [Acal_train,Wst] = stnmf(X_train,n_m,n_e_train);
%     disp(['Found ' int2str(n_m) ' spatiotemporal modules.']);
    % Obtain activation coefficients from test set for given modules
    Acal_test = stnmf(X_test,n_m,n_e_test,Wst);
    
    
elseif strcmp(opts.factorization,'SbyT')

    % Apply space-by-time NMF to factorize data
%     disp(['Optimizing numbers of temporal and spatial modules.'...
%         ' This can take a while...']);
    if opts.optimizeModules == true
    % Find optimal number of spatiotemporal modules depending on stimulus
    % classification
    [n_tm,n_sm] = select_n_tm_n_sm(X_train,groups_train,n_e_train,X_test,...
        groups_test,n_e_test);
    else
        n_tm = opts.nModulesSbyT(1);
        n_sm = opts.nModulesSbyT(2);
    end
    % Obtain temporal and spatial modules from training set
    [Acal_train,Wi,Wb,vaf,err] = sbtnmf(X_train,n_tm,n_sm,n_e_train);
%     disp(['Found ' int2str(n_tm) ' temporal and ' int2str(n_tm)...
%         ' spatial modules.']);
    % Obtain activation coefficients from test set for given modules
    Acal_test = sbtnmf(X_test,n_tm,n_sm,n_e_test,Wi,Wb);

end

out.activationCoeffsTrain = Acal_train;
out.activationCoeffsTest = Acal_test;
out.factorization = opts.factorization;
out.optimizeModules = opts.optimizeModules;
if strcmp(out.factorization,'ST')
    out.nModules = n_m;
    out.modules = Wst;
elseif strcmp(out.factorization,'SbyT')
    out.nModulesSpace = n_sm;
    out.nModulesTime = n_tm;
    out.modulesSpace = Wb;
    out.modulesTime = Wi;
    out.vaf = vaf;
    out.err = err;
end

end

