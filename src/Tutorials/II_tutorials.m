%
%  This source code is part of:
%  NIT - Neuroscience Information Toolbox
%  Copyright (C) 2020  Roberto Maffulli, Miguel Angel Casal Santiago
%
%  This program is free software: you can redistribute it and/or modify
%  it under the terms of the GNU General Public License as published by
%  the Free Software Foundation, either version 3 of the License, or
%  (at your option) any later version.
%
%  This program is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%  GNU General Public License for more details.
% 
%  You should have received a copy of the GNU General Public License
%  along with this program.  If not, see <https://www.gnu.org/licenses/>.
%
%%% # II tutorials
%%% ## Tutorial 1
%%% Tutorial 1 shows how to use NIT to calculate Intersection Information with 1D rate coding
clear all
close all
clc

dt = 1/50;
trialendtime = 1;
t_trial = 0:dt:trialendtime;
nStimuli = 2;
nTrials = 20;

% generate stimuli
mu = .2;  % mu for the gaussian
sigmaT = [.05,.05];  % sigma for each stimulus
rate = [10 50];  % peak rate for each stimulus
stimuli = nan(nStimuli,length(t_trial));
for i = 1:nStimuli
    signal = normpdf(t_trial,mu,sigmaT(i));
    stimuli(i,:) =  rate(i) * signal / max(signal);  %normalize for peak
end

% generate responses and choice (spike count)
R = [];
S = [];
C = [];
for i = 1:nStimuli
    for j = 1:nTrials
        R = [R sum(poisson_spike_gen(t_trial, stimuli(i,:), 0))];
        S = [S i];
    end
end
C = zeros(size(R));
C(R > 2) = 1;

% calculate II with the new implementation
ii_opts.bias = 'qe';
ii_opts.bin_method_activity = 'eqpop';
ii_opts.n_bins_activity = 2;
ii_opts.btsp = 10;
ii_opts.btsp_variables = {'R','S'};
ii_opts.btsp_type = {'all','all'};
ii_opts.btsp_bias = {'naive','naive'};
[II, PID_SR_C, PID_RC_S] = II(S, R, C, ii_opts);

fprintf("II biased =  %.8e\n", II.biased.value) 
fprintf("PID_SR_C shared biased =  %.8e\n", PID_SR_C.biased.shared.value)
fprintf("PID_SR_C uniqueR biased =  %.8e\n", PID_SR_C.biased.uniqueR.value)
fprintf("PID_SR_C uniqueS biased =  %.8e\n", PID_SR_C.biased.uniqueS.value)
fprintf("PID_SR_C complementary biased  =  %.8e\n", PID_SR_C.biased.complementary.value)
fprintf("PID_RC_S shared biased =  %.8e\n", PID_RC_S.biased.shared.value)
fprintf("PID_RC_S uniqueR biased =  %.8e\n", PID_RC_S.biased.uniqueR.value)
fprintf("PID_RC_S uniqueC biased =  %.8e\n", PID_RC_S.biased.uniqueC.value)
fprintf("PID_RC_S complementary biased =  %.8e\n", PID_RC_S.biased.complementary.value)
