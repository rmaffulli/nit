%%% Tutorial showing how to calculate conditional Feature Information Transfer using NIT
%%%
clear; clc;
rng(1)

nTrials = 10000;
noiseStr = 0.5;

S = binornd(1,0.5,1,nTrials); % Binomial stimulus

% Generating neural responses  S -> X -> Z -> Y
% We expect significant FIT about S from X to Y and non-significant FIT conditioned on Z
X_past = S + noiseStr*randn(1,nTrials); 
Z_past = X_past + noiseStr*randn(1,nTrials);
Y_pres = Z_past + noiseStr*randn(1,nTrials);
Y_past = noiseStr*randn(1,nTrials);

% Define information options
opts.verbose = false;
opts.method = "dr";
opts.bias = 'naive';
opts.bin_methodX = 'eqpop';
opts.bin_methodY = 'eqpop';
opts.n_binsX = 5;
opts.n_binsY = 5;
opts.n_binsZ = 3;
opts.bin_methodS = 'none';
opts.btsp = 100;
opts.btsp_variables = {'hX'};
opts.btsp_type = {'Sconditioned'};
opts.btsp_bias = {'naive'};

% Compute FIT(S:X->Y|Z)
[cFIT]=cFIT(S, X_past, Y_past, Y_pres, Z_past, opts); 
disp(['Measured cFIT = ', num2str(cFIT.biased.value) , ' pval = ', num2str(mean(cFIT.biased.btsp{1} >= cFIT.biased.value)+1/opts.btsp)]);
