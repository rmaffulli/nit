%
%  This source code is part of:
%  NIT - Neuroscience Information Toolbox
%  Copyright (C) 2020  Roberto Maffulli, Miguel Angel Casal Santiago
%
%  This program is free software: you can redistribute it and/or modify
%  it under the terms of the GNU General Public License as published by
%  the Free Software Foundation, either version 3 of the License, or
%  (at your option) any later version.
%
%  This program is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%  GNU General Public License for more details.
% 
%  You should have received a copy of the GNU General Public License
%  along with this program.  If not, see <https://www.gnu.org/licenses/>.
%
%%% # PID tutorials
%%% ## Tutorial 1
%%% Tutorial 1 shows how to use NIT to calculate PID with 1D rate coding
clear all
close all
clc
rng("default")

dt = 1/50;
trialendtime = 1;
t_trial = 0:dt:trialendtime;
nStimuli = 2;
nTrials = 20;

% generate stimuli
mu = .2;  % mu for the gaussian
sigmaT = [.05,.05];  % sigma for each stimulus
rate = [10 50];  % peak rate for each stimulus
stimuli = nan(nStimuli,length(t_trial));
for i = 1:nStimuli
    signal = normpdf(t_trial,mu,sigmaT(i));
    stimuli(i,:) =  rate(i) * signal / max(signal);  %normalize for peak
end

% generate responses and choice (spike count)
R = [];
S = [];
C = [];
for i = 1:nStimuli
    for j = 1:nTrials
        R = [R sum(poisson_spike_gen(t_trial, stimuli(i,:), 0))];
        S = [S i];
    end
end
C = zeros(size(R));
C(R > 2) = 1;


% calculate PID
pid_opts.bias = 'qe';
pid_opts.bin_methodX1 = 'eqpop';
pid_opts.n_binsX1 = 2;
pid_opts.btsp = 100;
pid_opts.btsp_variables = {'X1','Y'};
pid_opts.btsp_type = {'all','all'};
pid_opts.btsp_bias = {'naive','naive'};
PID = PID(S, R, C, pid_opts);

fprintf("PID_SR_C shared biased =  %.8e\n", PID.biased.shared.value)
fprintf("PID_SR_C uniqueR biased =  %.8e\n", PID.biased.uniqueX1.value)
fprintf("PID_SR_C uniqueS biased =  %.8e\n", PID.biased.uniqueX2.value)
fprintf("PID_SR_C complementary biased  =  %.8e\n", PID.biased.complementary.value)
fprintf("PID_RC_S shared unbiased =  %.8e\n", PID.unbiased.shared.value)
fprintf("PID_RC_S uniqueR unbiased =  %.8e\n", PID.unbiased.uniqueX1.value)
fprintf("PID_RC_S uniqueC unbiased =  %.8e\n", PID.unbiased.uniqueX2.value)
fprintf("PID_RC_S complementary unbiased =  %.8e\n", PID.unbiased.complementary.value)