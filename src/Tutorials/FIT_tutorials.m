%%% Tutorial showing how to calculate conditional Feature Information Transfer using NIT
%%%
clear; clc;
rng(1)

nTrials = 10000;
noiseStr = 0.5;

S = binornd(1,0.5,1,nTrials); % Binomial stimulus

% Generating neural responses  S -> X -> Z -> Y
% We expect significant FIT about S from X to Y and non-significant FIT conditioned on Z
X_past = S + noiseStr*randn(1,nTrials); 
Z_past = X_past + noiseStr*randn(1,nTrials);
Y_pres = Z_past + noiseStr*randn(1,nTrials);
Y_past = noiseStr*randn(1,nTrials);

% Define information options
opts.verbose = false;
opts.method = "dr";
opts.bias = 'naive';
opts.bin_methodX = 'eqpop';
opts.bin_methodY = 'eqpop';
opts.n_binsX = 5;
opts.n_binsY = 5;
opts.bin_methodS = 'none';
opts.btsp = 100;
opts.btsp_variables = {'hX'};
opts.btsp_type = {'Sconditioned'};
opts.btsp_bias = {'naive'};

% Compute FIT(S:X->Y)
[FIT]=FIT(S, X_past, Y_past, Y_pres,opts);
disp(['Measured FIT = ', num2str(FIT.biased.value) , ' pval = ', num2str(mean(FIT.biased.btsp{1} >= FIT.biased.value))]);