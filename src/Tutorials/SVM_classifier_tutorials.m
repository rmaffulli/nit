%
%  This source code is part of:
%  NIT - Neuroscience Information Toolbox
%  Copyright (C) 2020  Roberto Maffulli, Miguel Angel Casal Santiago
%
%  This program is free software: you can redistribute it and/or modify
%  it under the terms of the GNU General Public License as published by
%  the Free Software Foundation, either version 3 of the License, or
%  (at your option) any later version.
%
%  This program is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%  GNU General Public License for more details.
% 
%  You should have received a copy of the GNU General Public License
%  along with this program.  If not, see <https://www.gnu.org/licenses/>.
%
%%% # SVM Classifier tutorials
%%% ## Tutorial 1
%%% Tutorial 1 shows how to use NIT to solve a binary classification
%%% problem with a non-linear decision boundary. This is not strictly
%%% within the scope of NIT but effective use of a classifier to infer e.g.
%%% stimuli from the neural responses can be applied to estimate the mutual
%%% information between the neural response and stimuli.

clear all
close all
clc

n_pts = 1000;
X = unifrnd(0,10,[n_pts,1]);
Y = unifrnd(0,10,[n_pts,1]);

S = ones(n_pts,1);

for i=1:n_pts
    X_i = X(i);
    Y_i = Y(i);
    if X_i > 7
        S(i) = 2;
    else
        if Y_i > 6
            S(i) = 2;
        else
            if Y_i > X_i^2
                S(i) = 2;
            end
        end
    end
end
opts.algorithm = 'linear_SVM';
opts.cv_type = 'KFold';
opts.K = 10;
opts.optimize_params = true;
[predictedLabels, labelsTest, dataTest, probs] = buildML([X Y],S,0.25,opts);

figure()
hold on
colors = ['k','r'];
for i = 1:2
    scatter(dataTest(predictedLabels == i,1),dataTest(predictedLabels == i,2),10,colors(i));
end
% real decision boundary
x_db = linspace(0,sqrt(6),100);
y_db = x_db.^2;
ylim([0 10]);
plot(x_db,y_db,'-','LineWidth',2,'Color','b');
plot([sqrt(6) 7],[6 6],'-','LineWidth',2,'Color','b');
plot([7 7],[0 6],'-','LineWidth',2,'Color','b');