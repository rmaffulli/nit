clear all
close all
clc

rng('default')

S = [2*ones(1,50),ones(1,50);2*ones(1,50),ones(1,50)];
R = [randi(10,1,50), 5 + randi(10,1,50)];
C = double(R > 8);

opts.n_bins_stimulus = 2;
opts.n_bins_activity = 2;
opts.n_bins_choice = 2;
opts.bias = 'le';
opts.max_draws_per_split_number = 50;
opts.btsp = 10;
opts.btsp_variables = {"R", "C"};
opts.btsp_type = {"all", "Sconditioned"};
opts.btsp_bias = {"naive","qe"};
opts.bin_method_stimulus = 'eqspace';
opts.bin_method_activity = 'eqspace';
opts.bin_method_choice = 'eqspace';
II_out = II(S, R(1,:), C, opts);

disp("II Test successful")
