% Test for the cFIT function

% This is a simple simulation of a parallel transmission of information
% in the following scheme
%
%   S -->  X   --> Y
%     \           /
%      \         /
%        - > Z ->
%
% Here both X and Z carry significant FIT and cFIT(X->Y|Z) is also significant

clear all
close all
clc

rng('default')
numOfStimuli = 3;
numberOfTrials = 2^14; 
simulationLength = 120;
%The stimulus is modelled as a gaussian bump in the activity
stimulusEffect = normpdf(-3:0.3:3, 0, 2) - min(normpdf(-3:0.3:3, 0, 2));
noiseLambda = 2;
maxDelay = 45;
bins = 5;
%The number of parallel processes you can start in your cluster/PC
doPval = 1;
numPermutation = 5;

%When the stimulus starts and ends
stimulusOnsetTime1 = 40;
stimulusOnsetTime2 = 60;
stimulusEndTime1 = stimulusOnsetTime1 + length(stimulusEffect) - 1;
stimulusEndTime2 = stimulusOnsetTime2 + length(stimulusEffect) - 1;

%Here we generate the correlated stimuli

timePoint = 70;
delay = 20;

stimulusStrength = 5; % originally 2, but this should decrease the overall SNR (increasing X and Z strengths as well)
Zstrength = 1; % originally 0:0.1:1
Xstrength = 2; % originally (0:0.2:2)

stimId = [0 5 10 15];

% Simulation of parallel transmission with confounder

eps = 5; % Parameter to make the stimulus information in X and Z not redundant
xs = [stimId(1)+eps, stimId(2)-eps, stimId(3)+eps, stimId(4)-eps];

stim = randi([0 numOfStimuli], numberOfTrials,1);
[Xstimulus, Zstimulus] = XZ_S_response(stim,stimId,xs); % here I modiied xs --> xs/5 to reduce I(S;Z) increasing SNR_Z
Zstimulus = Zstimulus';
Xstimulus = Xstimulus';

% Noise generation
noiseAmplitude = 1;
NX = -noiseAmplitude+2*noiseAmplitude*rand(simulationLength,numberOfTrials);
NZ = -noiseAmplitude+2*noiseAmplitude*rand(simulationLength,numberOfTrials);
NY = -noiseAmplitude+2*noiseAmplitude*rand(simulationLength,numberOfTrials);

signal1 = stimulusStrength*ones(simulationLength, numberOfTrials);
signal2 = stimulusStrength*ones(simulationLength, numberOfTrials);
signal2baseline = 2*stimulusStrength;
signalZ = stimulusStrength*ones(simulationLength, numberOfTrials);

signal1(stimulusOnsetTime1:stimulusEndTime1, :) = signal1(stimulusOnsetTime1:stimulusEndTime1, :) + stimulusEffect' * Xstimulus' * stimulusStrength;
signal1 = signal1 .* 2;

signalZ(stimulusOnsetTime1:stimulusEndTime1, :) = signalZ(stimulusOnsetTime1:stimulusEndTime1, :) + stimulusEffect' * Zstimulus' * stimulusStrength;
signalZ = signalZ.*2;

%All dynamics (signals1 and 2) are a Poisson process with a noisy 
%time-dependent mean (activity1a, 1b and 2). The noise is additive to 
%the mean.
activity1 = poissrnd(signal1)+NX;
activityZ = poissrnd(signalZ)+NZ;

final_activity1 = mean(activity1,2);
final_activityZ = mean(activityZ,2);

signal2(stimulusOnsetTime2:stimulusEndTime2, :) = signal2(stimulusOnsetTime2:stimulusEndTime2, :) + Xstrength*(activity1(stimulusOnsetTime1:stimulusEndTime1, :) - 1) / 60 * stimulusStrength + Zstrength*(activityZ(stimulusOnsetTime1:stimulusEndTime1, :)-1)/60*stimulusStrength;
signal2 = signal2 .* 2;

activity2 = poissrnd(signal2)+NY;
final_activity2 = mean(activity2,2);

%% FIT computation

X = activity1(timePoint-delay, :);
Y = activity2(timePoint, :);
hY = activity2(timePoint - delay, :);
hZ = activityZ(timePoint - delay, :);

opts.n_binsX = bins;
opts.n_binsY = bins;
opts.n_binsZ = bins;
opts.n_binsS = 2;
opts.bias = 'qe';
opts.bin_method_X = 'eqpop';
opts.bin_method_Y = 'eqpop';
opts.bin_method_Z = 'eqpop';
opts.bin_method_S = 'none';
opts.max_draws_per_split_number = 200;
opts.nullhyp = 1;
opts.nh_perm = 100;

[fitx] = FIT(stim', X, hY, Y, opts);
[fitz] = FIT(stim', hZ, hY, Y, opts);
[cfit] = cFIT(stim', X, hY, Y, hZ, opts);

function [xstim,zstim] = XZ_S_response(stim,xs,zs)
% Auxiliary function for the encoding of stimulus in X and Y in the cFIT
% test script
Ntrials = numel(stim);

xstim = zeros(1,Ntrials);
zstim = zeros(1,Ntrials);

for idx = 1:Ntrials
    switch stim(idx)
        case 0
            xstim(idx) = xs(1);
            zstim(idx) = zs(1);
        case 1
            xstim(idx) = xs(2);
            zstim(idx) = zs(2);
        case 2
            xstim(idx) = xs(3);
            zstim(idx) = zs(3);
        case 3
            xstim(idx) = xs(4);
            zstim(idx) = zs(4);
        case 4
            xstim(idx) = xs(5);
            zstim(idx) = zs(5);
    end
end

end

